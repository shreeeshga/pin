/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.utils;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import in.shreesh.pin.R;
import in.shreesh.pin.api.PinBoardAPIPost;
import in.shreesh.pin.api.PinBoardAPIPostDelete;
import in.shreesh.pin.api.PinBoardAPITagDelete;
import in.shreesh.pin.provider.PinBoardContract;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.HashMap;

public class PinCursorUtils {

    public static void markRead(final Context context, final String url) {
        new PinBoardAPIPost.Builder(Preferences.getToken(context)).setUrl(url)
                .updatePost(new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> stringStringHashMap, Response response) {
                        context.getContentResolver()
                                .update(PinBoardContract.POSTS_CONTENT_URI, getPinContentValues(url),
                                        PinBoardContract.Post.COLUMN_NAME_HREF + " = ?", new String[]{url});
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(context, R.string.pin_update_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public static ContentValues getPinContentValues(String url) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_HREF, url);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_TO_READ, "no");

        return contentValues;
    }


    public static void deleteTag(final Context context, final String tagName) {
        new AlertDialog.Builder(context).setTitle(Utils.getString(context, R.string.confirmTitle))
                .setMessage(Utils.getString(context, R.string.confirmDeleteMessage))
                .setPositiveButton(Utils.getString(context, R.string.positiveText),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                eraseTag(context, tagName);
                            }
                        })
                .create()
                .show();
    }


    public static void eraseTag(final Context context, final String tag) {
        new PinBoardAPITagDelete.Builder(Preferences.getToken(context), tag).delete(
                new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> stringStringHashMap, Response response) {
                        if (!stringStringHashMap.get("result").equalsIgnoreCase("done")) {
                            Toast.makeText(context, R.string.tag_delete_error, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        context.getContentResolver()
                                .delete(PinBoardContract.TAGS_CONTENT_URI,
                                        PinBoardContract.Tag.COLUMN_NAME_TAG + " = ?", new String[]{tag});
                        context.getContentResolver().notifyChange(PinBoardContract.TAGS_CONTENT_URI, null, false);
                        Toast.makeText(context, R.string.tag_deleted, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public static void deletePin(final Context context, final String url) {
        new AlertDialog.Builder(context).setTitle(Utils.getString(context, R.string.confirmTitle))
                .setMessage(Utils.getString(context, R.string.confirmDeleteMessage))
                .setPositiveButton(Utils.getString(context, R.string.positiveText),
                                   new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialogInterface, int i) {
                                           erasePin(context, url);
                                       }
                                   })
                .create()
                .show();
    }

    public static void erasePin(final Context context, final String url) {
        new PinBoardAPIPostDelete.Builder(Preferences.getToken(context), url).delete(
                new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> stringStringHashMap, Response response) {
                        if (!stringStringHashMap.get("result_code").equalsIgnoreCase("done")) {
                            Toast.makeText(context, stringStringHashMap.get("result_code"), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        context.getContentResolver()
                                .delete(PinBoardContract.POSTS_CONTENT_URI,
                                        PinBoardContract.Post.COLUMN_NAME_HREF + " = ?", new String[]{url});
                        context.getContentResolver().notifyChange(PinBoardContract.POSTS_CONTENT_URI, null, false);
                        Toast.makeText(context, R.string.pin_deleted, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                    }
                });


    }

    public static ContentValues getNewPinContentValues(Context context, String url, String desc, String tag, String date,String note) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(PinBoardContract.Post.COLUMN_NAME_HREF, url);
            contentValues.put(PinBoardContract.Post.COLUMN_NAME_DESC, desc);
            contentValues.put(PinBoardContract.Post.COLUMN_NAME_NOTES, note);
            contentValues.put(PinBoardContract.Post.COLUMN_NAME_TO_READ, "yes");
            contentValues.put(PinBoardContract.Post.COLUMN_NAME_TAGS, tag);
            contentValues.put(PinBoardContract.Post.COLUMN_NAME_SHARED, "yes");
            contentValues.put(PinBoardContract.Post.COLUMN_NAME_TIME, date);


            return contentValues;
    }
}
