/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import in.shreesh.pin.R;

import java.util.LinkedHashSet;
import java.util.Set;

public class Preferences {

    public static String getToken(Context context) {
        SharedPreferences settings = context.getApplicationContext()
                .getSharedPreferences(Utils.PREFERENCE_FILE_NAME, 0);
        return settings.getString(Utils.PREFERENCE_TOKEN, "");
    }


    public static void setToken(Context context, String apiToken) {
        SharedPreferences settings = context.getApplicationContext()
                .getSharedPreferences(Utils.PREFERENCE_FILE_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Utils.PREFERENCE_TOKEN, apiToken);
        editor.commit();
    }

    public static String getLastUpdated(Context context) {
        SharedPreferences settings = context.getApplicationContext()
                .getSharedPreferences(Utils.PREFERENCE_FILE_NAME, 0);
        return settings.getString(Utils.PREFERENCE_LAST_UPDATE, "");
    }

    public static void setLastUpdated(Context context, String dateUTF) {
        SharedPreferences settings = context.getApplicationContext()
                .getSharedPreferences(Utils.PREFERENCE_FILE_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Utils.PREFERENCE_LAST_UPDATE, dateUTF);
        editor.commit();
    }


    public static void setUserName(Context context, String username) {
        SharedPreferences settings = context.getApplicationContext()
                .getSharedPreferences(Utils.PREFERENCE_FILE_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Utils.PREFERENCE_USER_NAME, username);
        editor.commit();
    }

    public static long getFreqency(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String val = preferences.getString("key_sync_frequency", "3600");
        return Long.valueOf(val);
    }

    public static String getArticlePreference(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString("key_open_url", context.getString(R.string.webview));
    }

    public static int getDefaultTab(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String val = preferences.getString("key_tab_default", "1");
        return Integer.valueOf(val);
    }

    public static boolean markPinRead(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean("key_read_default", false);
    }

    public static void setFeedTagSet(Context context, Set<String> tags) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet("key_tag_list", tags);
        editor.commit();
    }

    public static Set<String> getFeedTagSet(Context context) {
        Set<String> defaultTagSet = new LinkedHashSet<String>();
        defaultTagSet.add("popular");
        defaultTagSet.add("recent");

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getStringSet("key_tag_list", defaultTagSet);
    }


}


