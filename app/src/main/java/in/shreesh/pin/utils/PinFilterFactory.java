/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.utils;

import android.net.Uri;
import android.os.Parcelable;
import in.shreesh.pin.provider.PinBoardContract;

public class PinFilterFactory {

    public static String getSelection(int index) {
        switch (index) {
            case 1:
                return PinBoardContract.Post.COLUMN_NAME_TO_READ + " = ?";
            case 2:
                return PinBoardContract.Post.COLUMN_NAME_SHARED + " = ?";
        }
        return null;
    }

    public static String[] getSelectionArgs(int index) {
        String[] val = null;
        switch (index) {
            case 1:
                val = new String[]{"yes"};
                break;
            case 2:
                val = new String[]{"no"};
                break;
        }
        return val;
    }

}
