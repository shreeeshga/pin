/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import in.shreesh.pin.R;
import in.shreesh.pin.activities.Login;
import in.shreesh.pin.activities.PinDetailView;
import in.shreesh.pin.provider.PinBoardContract;
import retrofit.RestAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utils {
    public static final String API_URL = "https://api.pinboard.in/v1/";
    public static final String FEED_URL = "https://feeds.pinboard.in/";
    public static final String PACKAGE = "in.shreesh.pin";
    public static final String PREFERENCE_FILE_NAME = PACKAGE + ".File";
    public static final String PREFERENCE_TOKEN = PACKAGE + ".Token";
    public static final String PREFERENCE_LAST_UPDATE = PACKAGE + ".Last.Update";
    public static final String PREFERENCE_USER_NAME = PACKAGE + ".UserName";
    public static final String UTC_STRING_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String TAG = "Utils";

    public static RestAdapter adapter() {
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(API_URL)
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .build();
        return adapter;
    }

    public static RestAdapter adapterForFeed() {
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(FEED_URL)
                .setLogLevel(RestAdapter.LogLevel.NONE)
                .build();
        return adapter;
    }

    public static RestAdapter adapterWithLogLevel(RestAdapter.LogLevel level) {
        RestAdapter adapter = new RestAdapter.Builder().setEndpoint(API_URL).setLogLevel(level).build();
        return adapter;
    }

    public static String getString(Context context, int res) {
        return context.getResources().getString(res);
    }

    public static String formatUTCDate(Context context, String dateString) {

        DateFormat df = new java.text.SimpleDateFormat(UTC_STRING_FORMAT);
        Date date = new Date();
        try {
            date = df.parse(dateString);
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
        return DateUtils.formatDateTime(context, date.getTime(), DateUtils.FORMAT_ABBREV_WEEKDAY);
    }

    public static Date getDateFromUTCString(String dateString) {

        DateFormat df = new java.text.SimpleDateFormat(UTC_STRING_FORMAT);
        Date date = new Date();
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
        return date;
    }

    public static String getUTCString(Context context, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(UTC_STRING_FORMAT, Locale.US);
        return sdf.format(date);
    }

    public static boolean equals(String str1, String str2) {
        if (str1 == null || str2 == null) return false;
        return str1.equals(str2);
    }


    public static void triggerRefresh(Context context) {
        Bundle b = new Bundle();
        Account[] accounts = AccountManager.get(context).getAccountsByType("in.shreesh.pin.PinBoard");

        if (accounts.length > 0) {
            b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

            Account account = accounts[0];
            if (!ContentResolver.isSyncActive(account, PinBoardContract.CONTENT_AUTHORITY)) {
                ContentResolver.requestSync(account, PinBoardContract.CONTENT_AUTHORITY, b);
            }
        } else {
            new AlertDialog.Builder(context).setMessage(R.string.error_no_accounts).create();
            Preferences.setToken(context, "");
            context.startActivity(new Intent(context, Login.class));
        }
    }

    public static String stringFromArray(List<String> list) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String s : list)
            stringBuilder.append(s).append(" ");
        return stringBuilder.toString();
    }

    public static void showURLView(Context context, String link, String desc) {
        if (Preferences.getArticlePreference(context).equals(context.getResources().getString(R.string.browser))) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(link));
            context.startActivity(intent);
        } else if (Preferences.getArticlePreference(context)
                .equals(context.getResources().getString(R.string.webview))) {
            Intent intent = new Intent(context, PinDetailView.class);
            intent.putExtra("url", link);
            intent.putExtra("title", desc);
            context.startActivity(intent);
        }
    }
}
