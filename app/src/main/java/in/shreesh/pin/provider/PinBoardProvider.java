/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class PinBoardProvider extends ContentProvider {
    public static final int ROUTE_POSTS = 1;
    public static final int ROUTE_POSTS_ID = 2;
    public static final int ROUTE_TAGS = 3;
    public static final int ROUTE_TAGS_ID = 4;
    public static final int ROUTE_NOTES = 5;
    public static final int ROUTE_NOTES_ID = 6;
    public static final int ROUTE_SUGGEST = 7;
    public static final int ROUTE_POPULAR = 8;
    public static final int ROUTE_RECENT = 9;
    private static final int ROUTE_POPULAR_POSTS_ID = 10;
    private static final int ROUTE_RECENT_POSTS_ID = 11;
    private static final String TAG = "in.shreesh.pin.provider.PinBoardProvider";
    private static final String AUTHORITY = PinBoardContract.CONTENT_AUTHORITY;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);



    static {
        sUriMatcher.addURI(AUTHORITY, "posts", ROUTE_POSTS);
        sUriMatcher.addURI(AUTHORITY, "posts/*", ROUTE_POSTS_ID);
        sUriMatcher.addURI(AUTHORITY, "tags", ROUTE_TAGS);
        sUriMatcher.addURI(AUTHORITY, "tags/*", ROUTE_TAGS_ID);
        sUriMatcher.addURI(AUTHORITY, "notes", ROUTE_NOTES);
        sUriMatcher.addURI(AUTHORITY, "notes/*", ROUTE_NOTES_ID);

        sUriMatcher.addURI(AUTHORITY, "popular", ROUTE_POPULAR);
        sUriMatcher.addURI(AUTHORITY, "popular/*", ROUTE_POPULAR_POSTS_ID);

        sUriMatcher.addURI(AUTHORITY, "recent", ROUTE_RECENT);
        sUriMatcher.addURI(AUTHORITY, "recent/*", ROUTE_RECENT_POSTS_ID);

        sUriMatcher.addURI(AUTHORITY, "search_suggest_query", ROUTE_SUGGEST);
    }



    PinBoardDatabase database;

    @Override
    public boolean onCreate() {
        database = new PinBoardDatabase(getContext());
        return true;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        {
            SQLiteDatabase db = database.getReadableDatabase();
            SelectionBuilder builder = new SelectionBuilder();
            int uriMatch = sUriMatcher.match(uri);
            switch (uriMatch) {
                case ROUTE_POSTS_ID:
                    String id = uri.getLastPathSegment();
                    builder.where(PinBoardContract.Post._ID + "=?", id);

                case ROUTE_POSTS:
                    builder.table(PinBoardContract.Post.TABLE_NAME).where(selection, selectionArgs);
                    Cursor cursor = builder.query(db, projection, sortOrder);

                    Context ctx = getContext();
                    cursor.setNotificationUri(ctx.getContentResolver(), uri);
                    return cursor;

                case ROUTE_TAGS_ID:
                    id = uri.getLastPathSegment();
                    builder.where(PinBoardContract.Tag._ID + "=?", id);

                case ROUTE_TAGS:
                    builder.table(PinBoardContract.Tag.TABLE_NAME).where(selection, selectionArgs);
                    cursor = builder.query(db, projection, sortOrder);

                    ctx = getContext();
                    cursor.setNotificationUri(ctx.getContentResolver(), uri);
                    return cursor;

                case ROUTE_SUGGEST:
                    String match = uri.getLastPathSegment();
                    builder.table(PinBoardContract.Post.TABLE_NAME)
                        .where(selection, selectionArgs);

                    cursor = builder.query(db, projection, sortOrder);
                    ctx = getContext();
                    cursor.setNotificationUri(ctx.getContentResolver(), uri);
                    return cursor;

                default:
                    throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_POSTS:
                return PinBoardContract.Post.CONTENT_TYPE;
            case ROUTE_POSTS_ID:
                return PinBoardContract.Post.CONTENT_ITEM_TYPE;
            case ROUTE_TAGS:
                return PinBoardContract.Tag.CONTENT_TYPE;
            case ROUTE_TAGS_ID:
                return PinBoardContract.Tag.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        {
            final SQLiteDatabase db = database.getWritableDatabase();
            assert db != null;
            final int match = sUriMatcher.match(uri);
            Uri result;
            switch (match) {
                case ROUTE_POSTS:
                    long id = db.insertOrThrow(PinBoardContract.Post.TABLE_NAME, null, contentValues);
                    result = Uri.parse(PinBoardContract.POSTS_CONTENT_URI + "/" + id);
                    break;
                case ROUTE_POSTS_ID:
                    throw new UnsupportedOperationException("Insert not supported on URI: " + uri);
                case ROUTE_TAGS:
                    id = db.insertOrThrow(PinBoardContract.Tag.TABLE_NAME, null, contentValues);
                    result = Uri.parse(PinBoardContract.TAGS_CONTENT_URI + "/" + id);
                    break;
                case ROUTE_TAGS_ID:
                    throw new UnsupportedOperationException("Insert not supported on URI: " + uri);

                default:
                    throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
            // Send broadcast to registered ContentObservers, to refresh UI.
            Context ctx = getContext();
            assert ctx != null;
            ctx.getContentResolver().notifyChange(uri, null, false);
            return result;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = database.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;

        switch (match) {
            case ROUTE_POSTS:
                count = builder.table(PinBoardContract.Post.TABLE_NAME).where(selection, selectionArgs).delete(db);
                break;
            case ROUTE_POSTS_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(PinBoardContract.Post.TABLE_NAME)
                               .where(PinBoardContract.Post._ID + "=?", id)
                               .where(selection, selectionArgs)
                               .delete(db);
                break;
            case ROUTE_TAGS:
                count = builder.table(PinBoardContract.Tag.TABLE_NAME).where(selection, selectionArgs).delete(db);
                break;
            case ROUTE_TAGS_ID:
                id = uri.getLastPathSegment();
                count = builder.table(PinBoardContract.Tag.TABLE_NAME).where(PinBoardContract.Tag._ID + "=?", id).where(
                        selection, selectionArgs).delete(db);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }


        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;

    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = database.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;

        switch (match) {
            case ROUTE_POSTS:
                count = builder.table(PinBoardContract.Post.TABLE_NAME).where(selection, selectionArgs).update(db,
                                                                                                               contentValues);
                break;
            case ROUTE_POSTS_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(PinBoardContract.Post.TABLE_NAME)
                               .where(PinBoardContract.Post._ID + "=?", id)
                               .where(selection, selectionArgs)
                               .update(db, contentValues);
                break;

            case ROUTE_TAGS:
                count = builder.table(PinBoardContract.Tag.TABLE_NAME).where(selection, selectionArgs).update(db,
                                                                                                              contentValues);
                break;
            case ROUTE_TAGS_ID:
                id = uri.getLastPathSegment();
                count = builder.table(PinBoardContract.Post.TABLE_NAME)
                               .where(PinBoardContract.Post._ID + "=?", id)
                               .where(selection, selectionArgs)
                               .update(db, contentValues);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Context ctx = getContext();
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;

    }


}
