/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class PinBoardContract {

    public static final String CONTENT_AUTHORITY = "in.shreesh.pin.provider.PinBoardProvider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String[] POSTS_PROJECTION = new String[]{PinBoardContract.Post._ID, PinBoardContract.Post.COLUMN_NAME_HASH, PinBoardContract.Post.COLUMN_NAME_DESC, Post.COLUMN_NAME_NOTES, PinBoardContract.Post.COLUMN_NAME_HREF, Post.COLUMN_NAME_TO_READ, Post.COLUMN_NAME_SHARED, Post.COLUMN_NAME_TAGS, PinBoardContract.Post.COLUMN_NAME_TIME};

    public static final int COLUMN_ID = 0;
    public static final int COLUMN_HASH = 1;
    public static final int COLUMN_DESC = 2;
    public static final int COLUMN_NOTES = 3;
    public static final int COLUMN_HREF = 4;
    public static final int COLUMN_TO_READ = 5;
    public static final int COLUMN_SHARED = 6;
    public static final int COLUMN_TAGS = 7;
    public static final int COLUMN_TIME = 8;

    public static final String[] TAGS_PROJECTION = new String[]{Tag.COLUMN_NAME_TAG, Tag.COLUMN_NAME_TAG_COUNT, Tag._ID};
    public static final Uri POSTS_CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("posts").build();
    public static final Uri SEARCH_CONTENT_URI = BASE_CONTENT_URI.buildUpon()
            .appendPath("search_suggest_query")
            .build();
    public static final Uri TAGS_CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("tags").build();
    public static final Uri NOTES_CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("notes").build();

    public static class Post implements BaseColumns {

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pin" +
                ".posts";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd" +
                ".pin" +
                ".post";


        public static final String TABLE_NAME = "posts";
        public static final String COLUMN_NAME_HREF = "href";
        public static final String COLUMN_NAME_DESC = "description";
        public static final String COLUMN_NAME_NOTES = "extended";
        public static final String COLUMN_NAME_HASH = "hash";
        public static final String COLUMN_NAME_TO_READ = "toread";
        public static final String COLUMN_NAME_SHARED = "shared";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_TAGS = "tags";
    }


    public static class Tag implements BaseColumns {

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pin" +
                ".tags";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd" +
                ".pin" +
                ".tag";

        public static final String TABLE_NAME = "tags";
        public static final String COLUMN_NAME_TAG = "name";
        public static final String COLUMN_NAME_TAG_COUNT = "count";
    }

    public static class Note implements BaseColumns {

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pin" +
                ".notes";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd" +
                ".pin" +
                ".note";


        public static final String TABLE_NAME = "notes";
        public static final String COLUMN_NAME_NOTE_ID = "note_id";
        public static final String COLUMN_NAME_HASH = "hash";
        public static final String COLUMN_NAME_LENGTH = "length";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_TEXT = "text";
        public static final String COLUMN_NAME_CREATED = "created";
        public static final String COLUMN_NAME_UPDATED = "updated";

    }
}
