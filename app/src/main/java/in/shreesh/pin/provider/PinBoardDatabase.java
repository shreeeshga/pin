/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PinBoardDatabase extends SQLiteOpenHelper {
    public static final String TAG = "PinBoardDatabase";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "posts.db";

    private static final String TYPE_TEXT = " TEXT";
    private static final String TYPE_INTEGER = " INTEGER";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_POSTS = "CREATE TABLE " + PinBoardContract.Post.TABLE_NAME + " (" +
            PinBoardContract.Post._ID + " INTEGER PRIMARY KEY," +
            PinBoardContract.Post.COLUMN_NAME_HASH + TYPE_TEXT + COMMA_SEP +
            PinBoardContract.Post.COLUMN_NAME_HREF + TYPE_TEXT + COMMA_SEP +
            PinBoardContract.Post.COLUMN_NAME_DESC + TYPE_TEXT + COMMA_SEP +
            PinBoardContract.Post.COLUMN_NAME_NOTES + TYPE_TEXT + COMMA_SEP +
            PinBoardContract.Post.COLUMN_NAME_TIME + TYPE_TEXT + COMMA_SEP +
            PinBoardContract.Post.COLUMN_NAME_TO_READ + TYPE_TEXT + COMMA_SEP +
            PinBoardContract.Post.COLUMN_NAME_SHARED + TYPE_TEXT + COMMA_SEP +
            PinBoardContract.Post.COLUMN_NAME_TAGS + TYPE_TEXT + ");";



    private static final String SQL_CREATE_TAGS = "CREATE TABLE " + PinBoardContract.Tag.TABLE_NAME + " (" +
            PinBoardContract.Tag._ID + " INTEGER PRIMARY KEY," +
            PinBoardContract.Tag.COLUMN_NAME_TAG + TYPE_TEXT + COMMA_SEP +
            PinBoardContract.Tag.COLUMN_NAME_TAG_COUNT + TYPE_TEXT + ");";

    private static final String SQL_DELETE_POSTS = "DROP TABLE IF EXISTS " + PinBoardContract.Post.TABLE_NAME + ";";
    private static final String SQL_DELETE_TAGS = "DROP TABLE IF EXISTS " + PinBoardContract.Tag.TABLE_NAME + ";";

    public PinBoardDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "creating database");
        db.execSQL(SQL_CREATE_POSTS);
        db.execSQL(SQL_CREATE_TAGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_POSTS);
        db.execSQL(SQL_DELETE_TAGS);
        onCreate(db);
    }

}
