/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.sync;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.*;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import in.shreesh.pin.api.*;
import in.shreesh.pin.model.FeedPost;
import in.shreesh.pin.model.Post;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PinBoardSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = "PinBoardSyncAdapter";
    private final ContentResolver contentResolver;


    public PinBoardSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        contentResolver = context.getContentResolver();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public PinBoardSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        contentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        checkForUpdates(syncResult);
    }

    private void checkForUpdates(SyncResult syncResult) {
        HashMap<String, String> response = new PinBoardAPIPostUpdate.Builder(
                Preferences.getToken(getContext())).getLastUpdate();
        final String serverUpdate = response.get("update_time");
        String lastUpdated = Preferences.getLastUpdated(getContext());
        if (TextUtils.isEmpty(lastUpdated) || Utils.getDateFromUTCString(serverUpdate)
                .after(Utils.getDateFromUTCString(lastUpdated))) {
            fetchPosts(syncResult);
            Preferences.setLastUpdated(getContext(), serverUpdate);
        }

        fetchTags();
    }

    private void fetchTags() {
        HashMap<String, String> tags = new PinBoardAPITags.Builder(Preferences.getToken(getContext())).fetchTags();
        try {
            syncTags(tags);
        } catch (RemoteException e) {
            Log.d(TAG, e.getLocalizedMessage());
        } catch (OperationApplicationException e) {
            Log.d(TAG, e.getLocalizedMessage());
        }

    }

    private void syncTags(HashMap<String, String> tags) throws RemoteException, OperationApplicationException {
        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

        batch.add(ContentProviderOperation.newDelete(PinBoardContract.TAGS_CONTENT_URI).build());
        for (String key : tags.keySet()) {
            batch.add(ContentProviderOperation.newInsert(PinBoardContract.TAGS_CONTENT_URI)
                              .withValue(PinBoardContract.Tag.COLUMN_NAME_TAG, key)
                              .withValue(PinBoardContract.Tag.COLUMN_NAME_TAG_COUNT, tags.get(key))
                              .build());
        }


        contentResolver.applyBatch(PinBoardContract.CONTENT_AUTHORITY, batch);
        contentResolver.notifyChange(PinBoardContract.TAGS_CONTENT_URI, null, false);
    }

    private void fetchPosts(SyncResult syncResult) {
        List<Post> posts = new PinBoardPostsAll.Builder(Preferences.getToken(getContext())).fetch();
        try {
            mergePosts(posts, syncResult);
        } catch (RemoteException e) {
            syncResult.databaseError = true;
            Log.d(TAG, e.getLocalizedMessage());
        } catch (OperationApplicationException e) {
            Log.d(TAG, e.getLocalizedMessage());
            syncResult.databaseError = true;
        }
    }


    private void mergePosts(List<Post> posts, SyncResult syncResult) throws RemoteException, OperationApplicationException {
        HashMap<String, Post> postMap = new HashMap<String, Post>();
        int id;
        String href;
        String title;
        String hash;
        String description;
        String note;
        String toRead;
        String shared;
        String time;
        String tags;

        for (Post post : posts)
            postMap.put(post.href, post);

        //Log.i(TAG, "Fetching local entries for merge");
        Cursor cursor = contentResolver.query(PinBoardContract.POSTS_CONTENT_URI, PinBoardContract.POSTS_PROJECTION,
                                              null, null, null);
        assert cursor != null;
        //Log.i(TAG, "Found " + cursor.getCount() + " local entries. Computing merge solution...");

        ArrayList<ContentProviderOperation> batch = new ArrayList<ContentProviderOperation>();

        while (cursor.moveToNext()) {
            syncResult.stats.numEntries++;
            id = cursor.getInt(PinBoardContract.COLUMN_ID);
            href = cursor.getString(PinBoardContract.COLUMN_HREF);
            hash = cursor.getString(PinBoardContract.COLUMN_HASH);
            description = cursor.getString(PinBoardContract.COLUMN_DESC);
            note = cursor.getString(PinBoardContract.COLUMN_NOTES);
            toRead = cursor.getString(PinBoardContract.COLUMN_TO_READ);
            shared = cursor.getString(PinBoardContract.COLUMN_SHARED);
            time = cursor.getString(PinBoardContract.COLUMN_TIME);
            tags = cursor.getString(PinBoardContract.COLUMN_TAGS);

            Post matchPost = postMap.get(href);
            if (matchPost != null) {
                postMap.remove(href);
                Uri existingURI = PinBoardContract.POSTS_CONTENT_URI.buildUpon()
                        .appendPath(Integer.toString(id))
                        .build();

                if (!equals(matchPost.description, description) || !equals(matchPost.tags, tags) || !equals(
                        matchPost.time, time) || !equals(matchPost.toread, toRead) || !equals(matchPost.shared,
                                                                                              shared)) {
                    //Log.i(TAG, "Scheduling update: " + existingURI);
                    batch.add(ContentProviderOperation.newUpdate(existingURI)
                                      .withValue(PinBoardContract.Post.COLUMN_NAME_DESC, description)
                                      .withValue(PinBoardContract.Post.COLUMN_NAME_TAGS, tags)
                                      .withValue(PinBoardContract.Post.COLUMN_NAME_TIME, time)
                                      .withValue(PinBoardContract.Post.COLUMN_NAME_TO_READ, toRead)
                                      .withValue(PinBoardContract.Post.COLUMN_NAME_SHARED, shared)
                                      .build());

                    syncResult.stats.numUpdates++;
                }

            } else {
                Uri deleteUri = PinBoardContract.POSTS_CONTENT_URI.buildUpon().appendPath(Integer.toString(id)).build();
                //Log.i(TAG, "Scheduling delete: " + deleteUri);
                batch.add(ContentProviderOperation.newDelete(deleteUri).build());
                syncResult.stats.numDeletes++;
            }
        }

        cursor.close();


        for (Post post : postMap.values()) {
            //Log.i(TAG, "Scheduling insert: entry_id=" + post.description);
            batch.add(ContentProviderOperation.newInsert(PinBoardContract.POSTS_CONTENT_URI)
                              .withValue(PinBoardContract.Post.COLUMN_NAME_HREF, post.href)
                              .withValue(PinBoardContract.Post.COLUMN_NAME_HASH, post.hash)
                              .withValue(PinBoardContract.Post.COLUMN_NAME_DESC, post.description)
                              .withValue(PinBoardContract.Post.COLUMN_NAME_NOTES, post.extended)
                              .withValue(PinBoardContract.Post.COLUMN_NAME_TAGS, post.tags)
                              .withValue(PinBoardContract.Post.COLUMN_NAME_TIME, post.time)
                              .withValue(PinBoardContract.Post.COLUMN_NAME_TO_READ, post.toread)
                              .withValue(PinBoardContract.Post.COLUMN_NAME_SHARED, post.shared)
                              .build());
            syncResult.stats.numInserts++;
        }

        //Log.i(TAG, "Merge solution ready. Applying batch update");

        contentResolver.applyBatch(PinBoardContract.CONTENT_AUTHORITY, batch);
        contentResolver.notifyChange(PinBoardContract.POSTS_CONTENT_URI, null, false);
    }


    private boolean equals(String s1, String s2) {
        if (s1 == null && s2 != null) return false;
        if (s1 != null && s2 == null) return false;
        if (s1 == null && s2 == null) return true;
        return s1.equals(s2);

    }
}
