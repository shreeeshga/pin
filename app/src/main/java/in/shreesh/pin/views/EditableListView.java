/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import in.shreesh.pin.R;
import in.shreesh.pin.adapters.EditableListAdapter;

/**
 * Created by shreesh on 7/23/14.
 */
public class EditableListView extends LinearLayout {
    Context context;
    ListView listView;
    private EditableListAdapter adapter;


    public EditableListView(Context context) {
        super(context);
        init(context);
    }

    public EditableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EditableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public EditableListAdapter getAdapter() {
        return adapter;
    }
    public void setList(List<String> items) {
        adapter = new EditableListAdapter(items,context);
        listView.setAdapter(adapter);
    }
    private void init(Context context) {
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.editable_list_view,this);
        listView = (ListView) findViewById(R.id.listView);

    }
}
