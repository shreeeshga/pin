/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.activities;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import in.shreesh.pin.R;
import in.shreesh.pin.api.PinBoardAPIToken;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;
import retrofit.RetrofitError;

public class Login extends AccountAuthenticatorActivity {

    public static final String RESULT_EXTRA_USERNAME = Utils.PACKAGE + ".username";
    public static final String RESULT_EXTRA_TOKEN = Utils.PACKAGE + ".auth_token";


    public final static String ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public final static String ARG_AUTH_TYPE = "AUTH_TYPE";
    public final static String PARAM_USER_SECRET = "USER_SECRET";
    public final static String ARG_ACCOUNT_NAME = "ACCOUNT_NAME";
    public static final String KEY_ERROR_MESSAGE = "ERR_MSG";

    private AccountManager mAccountManager;
    private String authTokenType;

    private String userName;
    private String password;
    private String token;

    private EditText userNameView;
    private EditText passwordView;
    private TextView loginStatusMessageView;

    private View loginFormView;
    private View loginStatusView;

    private UserLoginTask authTask;
    private String accountType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        mAccountManager = AccountManager.get(getBaseContext());
        authTokenType = getIntent().getStringExtra(ARG_AUTH_TYPE);
        accountType = getIntent().getStringExtra(ARG_ACCOUNT_TYPE);

        setupActionBar();
        findViews();
    }

    private void findViews() {
        userNameView = (EditText) findViewById(R.id.userName);
        passwordView = (EditText) findViewById(R.id.password);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        loginFormView = findViewById(R.id.login_form);
        loginStatusView = findViewById(R.id.login_status);
        loginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    public void attemptLogin() {
        if (authTask != null) {
            return;
        }

        userNameView.setError(null);
        passwordView.setError(null);
        password = passwordView.getText().toString();
        userName = userNameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            focusView = passwordView;
            cancel = true;
        } else if(TextUtils.isEmpty(userName)) {
            userNameView.setError(getString(R.string.error_field_required));
            focusView = userNameView;
            cancel =true;
        }

        if (cancel) {
            // focus on error field
            focusView.requestFocus();
        } else {

            loginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);

            authTask = new UserLoginTask();
            authTask.execute(getBasicAuthHeader());
        }
    }

    private  String  getBasicAuthHeader() {
        String token = userName + ":" + password;
        return "Basic " + Base64.encodeToString(token.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginStatusView.setVisibility(View.VISIBLE);
            loginStatusView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(
                    new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            loginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    }
            );

            loginFormView.setVisibility(View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(
                    new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    }
            );
        } else {
            loginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public class UserLoginTask extends AsyncTask<String, Void, Intent> {
        private final String TAG = UserLoginTask.class.getSimpleName();

        @Override
        protected Intent doInBackground(String... params) {

            PinBoardAPIToken.TokenResponse response;
            Bundle data = new Bundle();
            try {
                response = new PinBoardAPIToken.Builder().fetch(params[0],"json");
                String token = new StringBuilder(userName).append(":").append(response.result).toString();

                data.putString(AccountManager.KEY_ACCOUNT_NAME, userName);
                data.putString(AccountManager.KEY_PASSWORD, userName);
                data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                data.putString(AccountManager.KEY_AUTHTOKEN, token);
                data.putString(PARAM_USER_SECRET,token);

            } catch (RetrofitError error) {
                data.putString(KEY_ERROR_MESSAGE, error.getMessage());

            }

            final android.content.Intent res = new android.content.Intent();
            res.putExtras(data);
            return res;
        }


        @Override
        protected void onPostExecute(final Intent response) {
            authTask = null;
            showProgress(false);

            if (!response.hasExtra(KEY_ERROR_MESSAGE)) {
                finishLogin(response);
            } else {
                passwordView.setError(getString(R.string.error_incorrect_password));
                passwordView.requestFocus();
            }
        }


        private void finishLogin(android.content.Intent intent) {

            String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            String accountPassword = intent.getStringExtra(PARAM_USER_SECRET);

            final Account account = new Account(accountName, accountType);

            String authtoken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            String authtokenType = intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);

            mAccountManager.addAccountExplicitly(account, accountPassword, null);
            ContentResolver.setIsSyncable(account, PinBoardContract.CONTENT_AUTHORITY, 1);
            ContentResolver.setSyncAutomatically(account, PinBoardContract.CONTENT_AUTHORITY, true);
            ContentResolver.addPeriodicSync(account, PinBoardContract.CONTENT_AUTHORITY, new Bundle(),Preferences.getFreqency(getApplicationContext()));


            mAccountManager.setAuthToken(account, authtokenType, authtoken);

            Preferences.setToken(getApplicationContext(),authtoken);
            setAccountAuthenticatorResult(intent.getExtras());

            intent = new Intent();
            intent.putExtra(RESULT_EXTRA_TOKEN,authtoken);
            intent.putExtra(RESULT_EXTRA_USERNAME,userName);
            setResult(RESULT_OK, intent);
            finish();
        }

        @Override
        protected void onCancelled() {
            authTask = null;
            showProgress(false);
        }
    }
}
