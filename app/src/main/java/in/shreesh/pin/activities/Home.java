/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.activities;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import in.shreesh.pin.R;
import in.shreesh.pin.fragments.PinTabsFragment;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;

public class Home extends FragmentActivity {
    private static final String FRAGMENT_TAG = "TAG_FRAGMENT";
    private static String TAG = Utils.PACKAGE + ".Home";
    private final int PINBOARD_LOGIN = 1;
    private String apiToken;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        checkLogin();
        if(getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG) == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            PinTabsFragment fragment = new PinTabsFragment();
            transaction.add(R.id.pin_fragment, fragment, FRAGMENT_TAG);
            transaction.commit();
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PINBOARD_LOGIN) {
            if (resultCode == RESULT_OK) {
                apiToken = data.getStringExtra(Login.RESULT_EXTRA_TOKEN);
                String username = data.getStringExtra(Login.RESULT_EXTRA_USERNAME);

                Preferences.setToken(this, apiToken);
                Preferences.setUserName(this, username);

                Utils.triggerRefresh(getApplicationContext());
            }
        }


    }


    private void checkLogin() {
        apiToken = Preferences.getToken(this);
        if (apiToken.isEmpty()) {

            final Intent intent = new Intent(this, Login.class);
            intent.putExtra(Login.ARG_ACCOUNT_TYPE, "in.shreesh.pin.PinBoard");
            intent.putExtra(Login.ARG_AUTH_TYPE, "BASIC_AUTH");

            startActivityForResult(intent, PINBOARD_LOGIN);
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}