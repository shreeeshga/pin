/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.widget.*;
import in.shreesh.pin.R;
import in.shreesh.pin.api.PinBoardAPIPost;
import in.shreesh.pin.api.PinBoardAPIPostSuggest;
import in.shreesh.pin.model.TagSuggetion;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;
import in.shreesh.pin.views.tags.TagListView;
import in.shreesh.pin.views.tags.TagView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AddPin extends Activity {
    private TextView urlTextView;
    private TextView titleTextView;
    private TextView descriptionTextView;
    private TextView dateChangedView;

    private CheckBox unreadPostView;
    private CheckBox privatePostView;
    private AutoCompleteTextView addTagsView;
    private TagListView tagsView;

    private String title;
    private String url;
    private TagListView recommendedTags;
    private TagListView popularTags;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post);
        findViews();

        if (parseIntent()) {
            initView();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_edit_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                if (validInput()) {
                    addPin();
                } else {
                    Toast.makeText(this, R.string.error_invalid_url, Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addPin() {
        final String title = titleTextView.getText().toString();
        final String url = urlTextView.getText().toString();
        final String description = descriptionTextView.getText().toString();
        final String isUnread = unreadPostView.isChecked() ? "yes" : "no";
        final String isShared = privatePostView.isChecked() ? "no" : "yes";
        final String tags = tagsView.getTagString();

        new PinBoardAPIPost.Builder(Preferences.getToken(getBaseContext())).setUrl(url)
                .setDescription(title)
                .setTag(tags)
                .setExtended(description)
                .setToread(isUnread)
                .setShared(isShared)
                .updatePost(new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> result, Response response) {
                        if (result.get("result_code").equalsIgnoreCase("done")) {
                            Uri uri = getContentResolver().insert(PinBoardContract.POSTS_CONTENT_URI,
                                                                  getContentValues(url, title, description, isUnread,
                                                                                   isShared, tags));

                            getContentResolver().notifyChange(PinBoardContract.POSTS_CONTENT_URI, null, false);
                            Toast.makeText(AddPin.this, R.string.post_added, Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(AddPin.this, result.get("result_code"), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(AddPin.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private ContentValues getContentValues(String url,
            String title,
            String description,
            String unread,
            String isShared,
            String tags) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_HREF, url);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_DESC, title);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_NOTES, description);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_TO_READ, unread);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_TAGS, tags);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_SHARED, isShared);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_TIME, Utils.getUTCString(this, new Date()));

        return contentValues;
    }

    private boolean validInput() {
        return url != null && URLUtil.isValidUrl(url);
    }

    private void initView() {
        fetchSuggests();
        initTagsView();

        unreadPostView.setChecked(true);
    }


    private void fetchSuggests() {
        final LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        new PinBoardAPIPostSuggest.Builder(Preferences.getToken(getApplicationContext()), url).fetchSuggest(
                new Callback<List<TagSuggetion>>() {
                    @Override
                    public void success(List<TagSuggetion> tagSuggetions, Response response) {
                        for (TagSuggetion tagSuggetion : tagSuggetions) {

                            List<String> recoTags = tagSuggetion.recommended;
                            List<String> popularTagList = tagSuggetion.popular;

                            if (recoTags != null) {
                                for (String tag : recoTags) {

                                    TagView tagView = (TagView) inflater.inflate(R.layout.tag,null);
                                    tagView.setText(tag);
                                    tagView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String text = ((TagView) v).getText().toString();
                                            tagsView.addTag(text);
                                            recommendedTags.removeView(v);
                                        }
                                    });
                                    recommendedTags.addView(tagView);
                                }
                            }
                            if (popularTagList != null) {
                                for (String tag : popularTagList) {
                                    TagView tagView = (TagView) inflater.inflate(R.layout.tag,null);
                                    tagView.setText(tag);
                                    tagView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String text = ((TagView) v).getText().toString();
                                            tagsView.addTag(text);
                                            popularTags.removeView(v);
                                        }
                                    });
                                    popularTags.addView(tagView);
                                }
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("EditPost", error.getLocalizedMessage());
                    }
                });
    }
    private void findViews() {
        titleTextView = (TextView) findViewById(R.id.titleText);
        descriptionTextView = (TextView) findViewById(R.id.descriptionText);
        urlTextView = (TextView) findViewById(R.id.urlText);
        tagsView = (TagListView) findViewById(R.id.tagsList);
        recommendedTags = (TagListView) findViewById(R.id.recommended_tags);
        popularTags = (TagListView) findViewById(R.id.popular_tags);
        dateChangedView = (TextView) findViewById(R.id.dateChanged);
        unreadPostView = (CheckBox) findViewById(R.id.unreadPin);
        privatePostView = (CheckBox) findViewById(R.id.privatePin);
    }

    public Cursor getCursor(CharSequence str) {
        String select = PinBoardContract.Tag.COLUMN_NAME_TAG + " LIKE ? ";
        String[] selectArgs = {"%" + str + "%"};
        String[] contactsProjection = new String[]{PinBoardContract.Tag._ID, PinBoardContract.Tag.COLUMN_NAME_TAG, PinBoardContract.Tag.COLUMN_NAME_TAG_COUNT,};

        return getContentResolver().query(PinBoardContract.TAGS_CONTENT_URI, contactsProjection, select, selectArgs,
                                          null);
    }

    private void initTagsView() {
        addTagsView = (AutoCompleteTextView) findViewById(R.id.addTags);
        int[] ids = new int[]{R.id.tagName, R.id.tagCount};
        SimpleCursorAdapter tagsListAdapter = new SimpleCursorAdapter(this, R.layout.tag_list_item, null,
                                                                      PinBoardContract.TAGS_PROJECTION, ids, 0);
        addTagsView.setAdapter(tagsListAdapter);
        tagsListAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence str) {
                return getCursor(str);
            }
        });

        tagsListAdapter.setCursorToStringConverter(new android.widget.SimpleCursorAdapter.CursorToStringConverter() {
            public CharSequence convertToString(Cursor cursor) {
                int index = cursor.getColumnIndex(PinBoardContract.Tag.COLUMN_NAME_TAG);
                return cursor.getString(index);
            }
        });

        addTagsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView text = (TextView) view.findViewById(R.id.tagName);
                Toast.makeText(AddPin.this, text.getText().toString(), Toast.LENGTH_SHORT).show();
                tagsView.addTag(text.getText().toString());
                addTagsView.setText("");
            }
        });
        addTagsView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_NULL && textView.getText().length() > 0) {
                    Toast.makeText(AddPin.this, textView.getText().toString(), Toast.LENGTH_SHORT).show();
                    tagsView.addTag(textView.getText().toString());
                    addTagsView.setText("");
                    return true;
                }

                return false;
            }
        });
    }

    private boolean parseIntent() {
        Intent intent = getIntent();
        String action = intent.getAction();
        String mime = intent.getType();

        if (Utils.equals(action, Intent.ACTION_SEND) && Utils.equals(mime, "text/plain")) {
            title = getIntent().getStringExtra(Intent.EXTRA_SUBJECT);
            url = getIntent().getStringExtra(Intent.EXTRA_TEXT);

            if (!android.webkit.URLUtil.isValidUrl(url)) {
                Toast.makeText(this, R.string.error_add_pin, Toast.LENGTH_SHORT).show();
                finish();
                return false;
            }


            titleTextView.setText(title);
            urlTextView.setText(url);

            Date date = new Date();
            dateChangedView.setText(DateUtils.formatDateTime(this, date.getTime(), DateUtils.FORMAT_ABBREV_WEEKDAY));
            Utils.getUTCString(this, date);

            return true;
        }

        Toast.makeText(this, R.string.error_mime_type, Toast.LENGTH_SHORT).show();
        finish();
        return false;
    }


}
