/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.activities;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import in.shreesh.pin.R;
import in.shreesh.pin.fragments.ArticleViewFragment;
import in.shreesh.pin.fragments.WebViewFragment;

public class PinDetailView extends Activity {

    private String url;
    private String TAG = "PinDetailView";
    private Menu optionsMenu;
    private String title;
    public static String WEBVIEW_FRAGMENT_TAG = "webview_fragment";
    public static String ARTICLE_FRAGMENT_TAG = "article_fragment";
    private WebViewFragment webviewFragment;
    private ArticleViewFragment articleFragment;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        optionsMenu = menu;
        inflater.inflate(R.menu.activity_webview, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_read:
                showArticle();
                break;
            case R.id.action_exit:
                showWebView();
                break;
            case R.id.action_browser:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                break;

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pindetail);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        url = getIntent().getStringExtra("url");
        title = getIntent().getStringExtra("title");
        setTitle(title);


        articleFragment = ArticleViewFragment.newInstance(url, title);
        webviewFragment = WebViewFragment.newInstance(url, title);
        showWebView();
    }


    private void showArticle() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);

        transaction.remove(webviewFragment);
        transaction.replace(R.id.fragmentContainer, articleFragment, ARTICLE_FRAGMENT_TAG);
        transaction.commit();
    }


    private void showWebView() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        transaction.remove(articleFragment);
        transaction.replace(R.id.fragmentContainer, webviewFragment, WEBVIEW_FRAGMENT_TAG);
        transaction.commit();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public Menu getOptionsMenu() {
        return optionsMenu;
    }
}
