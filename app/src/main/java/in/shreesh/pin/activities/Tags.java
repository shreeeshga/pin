/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.activities;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import in.shreesh.pin.R;
import in.shreesh.pin.adapters.TagAdapter;
import in.shreesh.pin.provider.PinBoardContract;

public class Tags extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private TagAdapter tagsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tags);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getLoaderManager().initLoader(0, null, this);

        ListView listView = (ListView) findViewById(android.R.id.list);
        int[] ids = new int[]{R.id.tagName, R.id.tagCount};

        tagsListAdapter = new TagAdapter(this, R.layout.tag_list_item, null, PinBoardContract.TAGS_PROJECTION,
                                                  ids, 0);
        listView.setAdapter(tagsListAdapter);
        listView.setEmptyView(findViewById(R.id.emptyTags));

        getLoaderManager().initLoader(0, null, this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        return new CursorLoader(this, PinBoardContract.TAGS_CONTENT_URI, PinBoardContract.TAGS_PROJECTION, null, null,
                                PinBoardContract.Tag.COLUMN_NAME_TAG + " COLLATE NOCASE ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (tagsListAdapter != null) tagsListAdapter.swapCursor(cursor);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        if (tagsListAdapter != null) tagsListAdapter.swapCursor(null);

    }
}
