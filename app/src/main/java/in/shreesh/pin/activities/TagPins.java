/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.activities;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;

import in.shreesh.pin.R;
import in.shreesh.pin.adapters.PinAdapter;
import in.shreesh.pin.provider.PinBoardContract;

/**
 * Created by shreesh on 7/24/14.
 */
public class TagPins extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String KEY_TAG_NAME = ".tag.name";
    private PinAdapter pinAdapter;
    private String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tags);

        tag = getIntent().getStringExtra(KEY_TAG_NAME);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle(tag);

        int[] ids = new int[]{R.id.title, R.id.date, R.id.tags};
        pinAdapter = new PinAdapter(this, R.layout.card_list_item, null, PinBoardContract.POSTS_PROJECTION, ids,
                0);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getLoaderManager().initLoader(0, null, this);
        setupList();
    }

    private void setupList() {
        ListView listView = (ListView) findViewById(android.R.id.list);
        listView.setAdapter(pinAdapter);
        listView.setEmptyView(findViewById(R.id.emptyTags));
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String filterSelection = PinBoardContract.Post.COLUMN_NAME_TAGS+ " LIKE ?";
        String[] filterSelectionArgs = new String[]{"%" + tag + "%"};

        return new CursorLoader(this, PinBoardContract.POSTS_CONTENT_URI, PinBoardContract.POSTS_PROJECTION, filterSelection, filterSelectionArgs,
                PinBoardContract.Post.COLUMN_NAME_TIME + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (pinAdapter != null) pinAdapter.swapCursor(cursor);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        if (pinAdapter != null) pinAdapter.swapCursor(null);

    }
}