/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.activities;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.widget.*;
import in.shreesh.pin.R;
import in.shreesh.pin.api.PinBoardAPIPost;
import in.shreesh.pin.api.PinBoardAPIPostSuggest;
import in.shreesh.pin.model.TagSuggetion;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;
import in.shreesh.pin.views.tags.TagListView;
import in.shreesh.pin.views.tags.TagView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.HashMap;
import java.util.List;

public class EditPost extends FragmentActivity implements TagListView.TagListener {
    public static final String EXTRA_POST_TITLE = Utils.PACKAGE + ".EXTRA_POST_TITLE";
    public static final String EXTRA_POST_URL = Utils.PACKAGE + ".EXTRA_POST_URL";
    private static final String TAG = "EditPost";
    private TextView urlTextView;
    private TextView titleTextView;
    private TextView descriptionTextView;
    private TextView dateChangedView;
    private CheckBox unreadPostView;
    private CheckBox privatePostView;
    private AutoCompleteTextView addTagsView;

    private TagListView tagsView;
    private TagListView recommendedTags;
    private TagListView popularTags;

    private String title;
    private String url;

    private boolean inEditMode;
    private int pinID;
    private String dateString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post);
        findViews();

        parseIntent();
        initView();
    }

    private void parseIntent() {
        Intent intent = getIntent();

        getActionBar().setDisplayHomeAsUpEnabled(true);
        title = intent.getStringExtra(EXTRA_POST_TITLE);
        url = intent.getStringExtra(EXTRA_POST_URL);
        inEditMode = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_edit_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                if (validInput()) {
                    saveAndUpdatePin();
                } else {
                    Toast.makeText(this, R.string.error_invalid_url, Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validInput() {
        return url != null && URLUtil.isValidUrl(url);
    }

    private void saveAndUpdatePin() {

        final String title = titleTextView.getText().toString();
        final String url = urlTextView.getText().toString();
        final String description = descriptionTextView.getText().toString();
        final String isUnread = unreadPostView.isChecked() ? "yes" : "no";
        final String isShared = privatePostView.isChecked() ? "no" : "yes";
        final String tags = tagsView.getTagString();

        new PinBoardAPIPost.Builder(Preferences.getToken(getBaseContext())).setUrl(url)
                .setDescription(title)
                .setDate(dateString)
                .setTag(tags)
                .setExtended(description)
                .setToread(isUnread)
                .setReplace("yes")
                .setShared(isShared)
                .updatePost(new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> result, Response response) {
                        if (result.get("result_code").equalsIgnoreCase("done")) {
                            int count = getContentResolver().update(PinBoardContract.POSTS_CONTENT_URI,
                                                                    getContentValues(url, title, description, isUnread,
                                                                                     isShared, tags),
                                                                    PinBoardContract.Post.COLUMN_NAME_HREF + " =?",
                                                                    new String[]{url});
                            if (count <= 0) {
                                Uri uri = getContentResolver().insert(PinBoardContract.POSTS_CONTENT_URI,
                                                                      getContentValues(url, title, description,
                                                                                       isUnread, isShared, tags));
                                Toast.makeText(EditPost.this, R.string.post_added, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(EditPost.this, R.string.post_updated, Toast.LENGTH_SHORT).show();
                            }

                            getContentResolver().notifyChange(PinBoardContract.POSTS_CONTENT_URI, null, false);
                            finish();
                        } else {
                            Toast.makeText(EditPost.this, result.get("result_code"), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(EditPost.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        return;
    }

    private void initView() {
        initTagsView();
        fetchSuggests();
        initFromCursor();
    }

    private void fetchSuggests() {
        final LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        new PinBoardAPIPostSuggest.Builder(Preferences.getToken(getApplicationContext()), url).fetchSuggest(
                new Callback<List<TagSuggetion>>() {
                    @Override
                    public void success(List<TagSuggetion> tagSuggetions, Response response) {
                        for (TagSuggetion tagSuggetion : tagSuggetions) {

                            List<String> recoTags = tagSuggetion.recommended;
                            List<String> popularTagList = tagSuggetion.popular;

                            if (recoTags != null) {
                                for (String tag : recoTags) {

                                    TagView tagView = (TagView) inflater.inflate(R.layout.tag, null);
                                    tagView.setText(tag);
                                    tagView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String text = ((TagView) v).getText().toString();
                                            tagsView.addTag(text);
                                            recommendedTags.removeView(v);
                                        }
                                    });
                                    recommendedTags.addView(tagView);
                                }
                            }
                            if (popularTagList != null) {
                                for (String tag : popularTagList) {
                                    TagView tagView = (TagView) inflater.inflate(R.layout.tag, null);
                                    tagView.setText(tag);
                                    tagView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String text = ((TagView) v).getText().toString();
                                            tagsView.addTag(text);
                                            popularTags.removeView(v);
                                        }
                                    });
                                    popularTags.addView(tagView);
                                }
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("EditPost", error.getLocalizedMessage());
                    }
                });
    }

    private void initFromCursor() {
        Cursor cursor = getContentResolver().query(PinBoardContract.POSTS_CONTENT_URI,
                                                   PinBoardContract.POSTS_PROJECTION,
                                                   PinBoardContract.Post.COLUMN_NAME_HREF + " = " + DatabaseUtils.sqlEscapeString(
                                                           url), null, null);
        try {
            if (cursor.moveToFirst()) {
                descriptionTextView.setText(
                        cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_NOTES)));
                urlTextView.setText(cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_HREF)));
                titleTextView.setText(cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_DESC)));

                dateString = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_TIME));
                dateChangedView.setText(Utils.formatUTCDate(this, dateString));
                String isUnread = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_TO_READ));
                String isShared = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_SHARED));

                unreadPostView.setChecked(Utils.equals(isUnread, "yes"));
                privatePostView.setChecked(Utils.equals(isShared, "no"));

                String[] tagString = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_TAGS))
                        .split(" ");

                for (String tag : tagString)
                    if (!tag.trim().isEmpty()) tagsView.addTag(tag);
            }
        } finally {
            cursor.close();
        }
    }

    private void initTagsView() {
        addTagsView = (AutoCompleteTextView) findViewById(R.id.addTags);
        int[] ids = new int[]{R.id.tagName, R.id.tagCount};
        SimpleCursorAdapter tagsListAdapter = new SimpleCursorAdapter(this, R.layout.tag_list_item, null,
                                                                      PinBoardContract.TAGS_PROJECTION, ids, 0);
        addTagsView.setAdapter(tagsListAdapter);
        tagsListAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence str) {
                return getCursor(str);
            }
        });

        tagsListAdapter.setCursorToStringConverter(new android.widget.SimpleCursorAdapter.CursorToStringConverter() {
            public CharSequence convertToString(Cursor cursor) {
                int index = cursor.getColumnIndex(PinBoardContract.Tag.COLUMN_NAME_TAG);
                return cursor.getString(index);
            }
        });

        addTagsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView text = (TextView) view.findViewById(R.id.tagName);

                tagsView.addTag(text.getText().toString());
                addTagsView.setText("");
            }
        });
        addTagsView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE && textView.getText().length() > 0) {

                    tagsView.addTag(textView.getText().toString());
                    addTagsView.setText("");
                    return true;
                }

                return false;
            }
        });


    }

    private void findViews() {
        titleTextView = (TextView) findViewById(R.id.titleText);
        descriptionTextView = (TextView) findViewById(R.id.descriptionText);
        urlTextView = (TextView) findViewById(R.id.urlText);
        tagsView = (TagListView) findViewById(R.id.tagsList);
        recommendedTags = (TagListView) findViewById(R.id.recommended_tags);
        popularTags = (TagListView) findViewById(R.id.popular_tags);
        dateChangedView = (TextView) findViewById(R.id.dateChanged);
        unreadPostView = (CheckBox) findViewById(R.id.unreadPin);
        privatePostView = (CheckBox) findViewById(R.id.privatePin);
    }

    public Cursor getCursor(CharSequence str) {
        String select = PinBoardContract.Tag.COLUMN_NAME_TAG + " LIKE ? ";
        String[] selectArgs = {"%" + str + "%"};
        String[] contactsProjection = new String[]{PinBoardContract.Tag._ID, PinBoardContract.Tag.COLUMN_NAME_TAG, PinBoardContract.Tag.COLUMN_NAME_TAG_COUNT,};

        return getContentResolver().query(PinBoardContract.TAGS_CONTENT_URI, contactsProjection, select, selectArgs,
                                          null);
    }

    private ContentValues getContentValues(String url, String title, String description, String unread, String isShared, String tags) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_HREF, url);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_DESC, title);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_NOTES, description);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_TO_READ, unread);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_TAGS, tags);
        contentValues.put(PinBoardContract.Post.COLUMN_NAME_SHARED, isShared);

        return contentValues;
    }

    @Override
    public void onAddedTag(String tag) {
        //empty
    }

    @Override
    public void onRemovedTag(String tag) {
        tagsView.removeTag(tag);
    }
}
