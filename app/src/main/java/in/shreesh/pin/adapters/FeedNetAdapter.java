/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.*;
import android.widget.*;
import in.shreesh.pin.R;
import in.shreesh.pin.activities.PinDetailView;
import in.shreesh.pin.api.PinBoardAPIPost;
import in.shreesh.pin.model.FeedPost;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FeedNetAdapter extends BaseAdapter {
    private List<FeedPost> items = new ArrayList<FeedPost>();
    private Context context;

    public FeedNetAdapter(Context context) {
        this.context = context;
    }

    public void setItems(List<FeedPost> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public FeedPost getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.card_list_item, parent, false);
            convertView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.card_normal));
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.tags = (TextView) convertView.findViewById(R.id.tags);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date);
            viewHolder.note = (TextView) convertView.findViewById(R.id.note);
            viewHolder.menuIcon = (ImageView) convertView.findViewById(R.id.itemMenu);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.title.setText(getItem(position).d);
        viewHolder.tags.setText(getItem(position).getTagString());
        if (!TextUtils.isEmpty(getItem(position).n)) {
            viewHolder.note.setText(getItem(position).n);
            viewHolder.note.setVisibility(View.VISIBLE);
        } else {
            viewHolder.note.setVisibility(View.GONE);
        }
        viewHolder.menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view, getItem(position));
            }
        });


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PinDetailView.class);
                intent.putExtra("url", getItem(position).u);
                intent.putExtra("title", getItem(position).d);
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    private void showMenu(View view, final FeedPost feedPost) {
        PopupMenu popup = new PopupMenu(context, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.actions_feed, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_copy:
                        createPin(feedPost);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();

    }

    private void createPin(FeedPost feedPost) {

        new PinBoardAPIPost.Builder(Preferences.getToken(context)).setUrl(feedPost.u)
                .setDescription(feedPost.d)
                .setDate(feedPost.dt)
                .setTag(Utils.stringFromArray(feedPost.t))
                .setReplace("no")
                .setToread("yes")
                .setExtended(feedPost.n)
                .updatePost(new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> result, Response response) {
                        if (result.get("result_code").equalsIgnoreCase("done")) {
                            Toast.makeText(context, R.string.post_copied, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, result.get("result_code"), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private static class ViewHolder {
        TextView title;
        TextView tags;
        TextView date;
        ImageView menuIcon;
        TextView note;
    }
}
