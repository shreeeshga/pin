/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.adapters;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.text.Editable;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import in.shreesh.pin.R;
import in.shreesh.pin.activities.TagPins;
import in.shreesh.pin.api.PinBoardAPITagUpdate;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.sync.PinBoardSyncAdapter;
import in.shreesh.pin.utils.PinCursorUtils;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by shreesh on 7/21/14.
 */
public class TagAdapter extends BaseCursorAdapter {
    public TagAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }


    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        final TextView tagName = (TextView) view.findViewById(R.id.tagName);
        TextView tagCount = (TextView) view.findViewById(R.id.tagCount);
        ImageView menu = (ImageView) view.findViewById(R.id.itemMenu);

        final String tag = cursor.getString(cursor.getColumnIndex(PinBoardContract.Tag.COLUMN_NAME_TAG));
        String count = cursor.getString(cursor.getColumnIndex(PinBoardContract.Tag.COLUMN_NAME_TAG_COUNT));

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(context, view, tag);
            }
        });

        tagName.setText(tag);
        tagCount.setText(count);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TagPins.class);
                intent.putExtra(TagPins.KEY_TAG_NAME,tag);
                context.startActivity(intent);
            }
        });
    }


    public void showMenu(final Context context, View v, final String tag) {
        PopupMenu popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.actions_tag, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_edit:
                        editTag(context, tag);
                        return true;
                    case R.id.action_delete:
                        PinCursorUtils.deleteTag(context, tag);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    private void editTag(final Context context, final String tag) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setTitle("Edit");

        final EditText input = new EditText(context);
        input.setText(tag);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final Editable value = input.getText();
                new PinBoardAPITagUpdate.Builder(Preferences.getToken(context), tag, value.toString()).update(new retrofit.Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> stringStringHashMap, Response response) {
                        int count = context.getContentResolver().update(PinBoardContract.TAGS_CONTENT_URI,
                                getContentValues(value.toString()),
                                PinBoardContract.Tag.COLUMN_NAME_TAG+ " =?",
                                new String[]{tag});
                        if (count > 0) {
                            Toast.makeText(context, R.string.tag_updated, Toast.LENGTH_SHORT).show();
                            Utils.triggerRefresh(context.getApplicationContext());
                        } else {
                            Toast.makeText(context, R.string.tag_update_error, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }


    private ContentValues getContentValues(String tag) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(PinBoardContract.Tag.COLUMN_NAME_TAG, tag);
        return contentValues;
    }

}
