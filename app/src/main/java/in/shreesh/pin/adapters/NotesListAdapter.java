/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import in.shreesh.pin.R;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.Utils;

public class NotesListAdapter extends SimpleCursorAdapter {
    private int layoutResource;
    private Context context;

    public NotesListAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.layoutResource = layout;
        this.context = context;

    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
            final TextView title = (TextView) view.findViewById(R.id.title);
            TextView date = (TextView) view.findViewById(R.id.date);
            ImageView menuIcon = (ImageView) view.findViewById(R.id.itemMenu);

            final String titleString = cursor.getString(cursor.getColumnIndex(PinBoardContract.Note.COLUMN_NAME_TITLE));
            final String note = cursor.getString(cursor.getColumnIndex(PinBoardContract.Note.COLUMN_NAME_TEXT));
            final String id = cursor.getString(cursor.getColumnIndex(PinBoardContract.Note.COLUMN_NAME_NOTE_ID));
            title.setText(titleString);

            date.setText(Utils.formatUTCDate(context, cursor.getString(
                    cursor.getColumnIndex(PinBoardContract.Note.COLUMN_NAME_CREATED))));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Intent intent = new Intent(context, EditNote.class);
                    //intent.putExtra("id", id);
                    //intent.putExtra("title", titleString);
                    //context.startActivity(intent);
                }
            });
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(layoutResource, parent, false);
        return view;
    }
}
