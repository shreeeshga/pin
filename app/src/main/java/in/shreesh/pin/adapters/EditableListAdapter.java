/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.shreesh.pin.R;

/**
 * Created by shreesh on 7/23/14.
 */
public class EditableListAdapter extends BaseAdapter {
    List<String> list = new ArrayList<String>();
    Context context;

    public EditableListAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public List<String> getList() {
        return list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public String getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void add(String item) {
        list.add(0,item);
        notifyDataSetChanged();
    }

    public void remove(int i) {
        list.remove(i);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.feed_row, viewGroup, false);
        }

        TextView textView = (TextView) view.findViewById(R.id.feed_name);
        ImageView imageView = (ImageView) view.findViewById(R.id.delete_icon);
        String tag;

        if (list.get(i).startsWith("t:"))
            tag = list.get(i).substring(2);
        else
            tag = list.get(i);
        textView.setText(tag);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(i);
                notifyDataSetChanged();
            }
        });

        return view;
    }
}
