/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import in.shreesh.pin.R;
import in.shreesh.pin.api.PinBoardAPIPost;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.PinCursorUtils;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FeedAdapter extends BaseCursorAdapter {

    public FeedAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    public static void createPin(final Context context, final String url, final String desc, final String tags, final String date, final String note) {

        new PinBoardAPIPost.Builder(Preferences.getToken(context)).setUrl(url)
                .setDescription(desc)
                .setExtended(note)
                .setReplace("no")
                .setToread("yes")
                .setTag(tags)
                .setDate(date)
                .updatePost(new Callback<HashMap<String, String>>() {
                    @Override
                    public void success(HashMap<String, String> result, Response response) {
                        if (result.get("result_code").equalsIgnoreCase("done")) {
                            context.getContentResolver()
                                    .insert(PinBoardContract.POSTS_CONTENT_URI,
                                            PinCursorUtils.getNewPinContentValues(context, url, desc, tags, date, note));
                            context.getContentResolver().notifyChange(PinBoardContract.POSTS_CONTENT_URI, null, false);
                            Toast.makeText(context, R.string.post_copied, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, result.get("result_code"), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = super.newView(context, cursor, parent);
        view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.card_background_feed));
        return view;
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView tags = (TextView) view.findViewById(R.id.tags);
        TextView date = (TextView) view.findViewById(R.id.date);
        ImageView menuIcon = (ImageView) view.findViewById(R.id.itemMenu);

        final String desc = cursor.getString(PinBoardContract.COLUMN_DESC);
        final String url = cursor.getString(PinBoardContract.COLUMN_HREF);
        final String tag = cursor.getString(PinBoardContract.COLUMN_TAGS);
        final String note = cursor.getString(PinBoardContract.COLUMN_NOTES);
        final String dateTime = cursor.getString(PinBoardContract.COLUMN_TIME);

        title.setText(desc);
        tags.setText(tag);
        date.setText(Utils.formatUTCDate(context, dateTime));

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(view, url, desc, tag, dateTime, note);
            }
        });


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showURLView(context, url, desc);
            }
        });
    }

    private void showMenu(View view, final String url, final String desc, final String tags, final String date, final String notes) {
        PopupMenu popup = new PopupMenu(context, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.actions_feed, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_copy:
                        createPin(context, url, desc, tags, date, notes);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();

    }
}
