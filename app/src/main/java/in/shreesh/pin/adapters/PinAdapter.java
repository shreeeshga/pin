/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import in.shreesh.pin.R;
import in.shreesh.pin.activities.EditPost;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.PinCursorUtils;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;

public class PinAdapter extends BaseCursorAdapter {


    public PinAdapter(Context context, int layout, Cursor cursor, String[] from, int[] to, int flags) {
        super(context, layout, cursor, from, to, flags);
        this.layoutResource = layout;
        this.context = context;
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = super.newView(context, cursor, parent);
        String notes = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_NOTES));

        if(!TextUtils.isEmpty(notes.trim())) {
            view.findViewById(R.id.note).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.note).setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        super.bindView(view,context,cursor);
        final TextView title = (TextView) view.findViewById(R.id.title);
        TextView tags = (TextView) view.findViewById(R.id.tags);
        TextView date = (TextView) view.findViewById(R.id.date);
        TextView note = (TextView) view.findViewById(R.id.note);
        ImageView menuIcon = (ImageView) view.findViewById(R.id.itemMenu);

        final String link = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_HREF));
        final String desc = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_DESC));
        final String notes = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_NOTES));

        title.setText(desc);
        tags.setText(cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_TAGS)));
        final String toread = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_TO_READ));
        String shared = cursor.getString(cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_SHARED));

        view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.card_normal));

        if (shared.equalsIgnoreCase("no")) {
            view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.card_private));

        }
        if (toread.equalsIgnoreCase("yes")) {
            view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.card_unread));
        }

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu(context, view, desc, link, toread.equalsIgnoreCase("yes"));
            }
        });

        date.setText(Utils.formatUTCDate(context, cursor.getString(
                cursor.getColumnIndex(PinBoardContract.Post.COLUMN_NAME_TIME))));

        note.setText(notes);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.showURLView(context, link, desc);
                if (Preferences.markPinRead(context))
                    PinCursorUtils.markRead(context, link);
            }
        });
    }

    public void showMenu(final Context context, View v, final String title, final String url, final boolean unread) {
        PopupMenu popup = new PopupMenu(context, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.actions_pins, popup.getMenu());

        if (unread) {
            popup.getMenu().findItem(R.id.action_mark_read).setVisible(true);
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_mark_read:
                        PinCursorUtils.markRead(context, url);
                        return true;
                    case R.id.action_edit:
                        edit(title, url);
                        return true;
                    case R.id.action_delete:
                        PinCursorUtils.deletePin(context, url);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    private void edit(String title, String url) {
        Intent intent = new Intent(context, EditPost.class);
        intent.putExtra(EditPost.EXTRA_POST_TITLE, title);
        intent.putExtra(EditPost.EXTRA_POST_URL, url);
        context.startActivity(intent);
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        notifyDataSetChanged();

    }
}
