/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.fragments;

import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.Image;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.boilerpipe.extractors.CommonExtractors;
import de.l3s.boilerpipe.sax.ImageExtractor;
import in.shreesh.pin.R;
import in.shreesh.pin.activities.PinDetailView;
import in.shreesh.pin.common.ArticleFormatter;
import in.shreesh.pin.utils.Utils;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class ArticleViewFragment extends Fragment {

    private static final int SHOW_ARTICLE = 1;
    public static final String TAG = "ArticleViewFragment";

    private TextView articleText;
    private ImageView articleImage;
    private String article;
    private List<Image> imgUrls;
    private CustomHandler handler = new CustomHandler();
    private Thread currentThread;
    private String url;
    private String title;
    private TextView articleTitle;
    private TextView articleImageAlt;
    private String imageAlt = "";
    private TextView emptyView;
    private LinearLayout articleContent;


    private class CustomHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SHOW_ARTICLE:
                    if (!imgUrls.isEmpty()) {
                        Picasso.with(getActivity()).load(imgUrls.get(0).getSrc()).centerCrop().resize(400,300).into(articleImage);
                        imageAlt = imgUrls.get(0).getAlt();
                    } else {
                        articleImage.setVisibility(View.GONE);
                    }

                    processText(article);
                    setRefreshActionButtonState(false);
                    break;
            }
        }
    }

    private void processText(String article) {
        articleTitle.setText(title);
        if (imageAlt != null && !imageAlt.isEmpty()) {
            articleImageAlt.setText(imageAlt);
        }

        ArticleFormatter articleFormatter =  new ArticleFormatter(title,article);
        articleText.setText(articleFormatter.getContent());
    }


    private class ArticleRunnable implements Runnable {

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            currentThread = Thread.currentThread();

            Log.i("ArticleViewFragment", "URL " + url);
            try {

                article = ArticleExtractor.INSTANCE.getText(new URL(url));
                imgUrls = ImageExtractor.INSTANCE.process(new URL(url), CommonExtractors.ARTICLE_EXTRACTOR);
                Collections.sort(imgUrls);

            } catch (BoilerpipeProcessingException e) {
                Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                return;
            } catch (SAXException e) {
                Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                return;
            } catch (MalformedURLException e) {
                Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                return;
            } catch (IOException e) {
                Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                return;
            }

            ArticleViewFragment.this.handler.sendEmptyMessage(ArticleViewFragment.this.SHOW_ARTICLE);
        }
    }

    public static ArticleViewFragment newInstance(String url, String title) {
        ArticleViewFragment fragment = new ArticleViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        bundle.putString("title", title);

        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        url = getArguments().getString("url");
        title = getArguments().getString("title");
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.articleview, container, false);
        articleText = (TextView) view.findViewById(R.id.article_text);
        articleImage = (ImageView) view.findViewById(R.id.article_image);
        articleTitle = (TextView) view.findViewById(R.id.article_title);
        articleImageAlt = (TextView) view.findViewById(R.id.article_image_alt);
        articleContent = (LinearLayout) view.findViewById(R.id.articleView);

        emptyView = (TextView) view.findViewById(R.id.emptyView);
        setRefreshActionButtonState(true);
        currentThread = new Thread(new ArticleRunnable());
        currentThread.start();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    private void setImmersiveMode() {
        int uiOptions = getActivity().getWindow().getDecorView().getSystemUiVisibility();
        if (Build.VERSION.SDK_INT >= 14) {
            uiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }


        if (Build.VERSION.SDK_INT >= 16) {
            uiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        if (Build.VERSION.SDK_INT >= 18) {
            uiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        getActivity().getWindow().getDecorView().setSystemUiVisibility(uiOptions);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        Menu optionsMenu = ((PinDetailView) getActivity()).getOptionsMenu();
        optionsMenu.findItem(R.id.action_read).setVisible(false);
        optionsMenu.findItem(R.id.action_browser).setVisible(false);
        optionsMenu.findItem(R.id.action_refresh).setVisible(false);
        optionsMenu.findItem(R.id.action_exit).setVisible(true);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setRefreshActionButtonState(false);

        currentThread.interrupt();
    }


    public void setRefreshActionButtonState(boolean refreshing) {
        if (refreshing) {
            emptyView.setVisibility(View.VISIBLE);
            articleContent.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            articleContent.setVisibility(View.VISIBLE);
        }
    }

}
