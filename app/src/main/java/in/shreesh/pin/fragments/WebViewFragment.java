/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.*;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import in.shreesh.pin.R;
import in.shreesh.pin.activities.PinDetailView;

public class WebViewFragment extends Fragment {

    private WebView client;
    private String url;
    private String title;

    public static WebViewFragment newInstance(String url, String title) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        bundle.putString("title", title);

        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        url = getArguments().getString("url");
        title = getArguments().getString("title");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.webview, container, false);
        client = (WebView) view.findViewById(R.id.webview);


        client.getSettings().setJavaScriptEnabled(true);
        client.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                setRefreshActionButtonState(true);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                setRefreshActionButtonState(false);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                setRefreshActionButtonState(false);
            }
        });

        client.loadUrl(url);

        return view;
    }

    @Override
    public void onDestroyView() {
        if (client != null) {
            ViewGroup parent = (ViewGroup) client.getParent();
            if (parent != null) {
                parent.removeView(client);
            }
            client.removeAllViews();
            client.destroy();
        }
        super.onDestroyView();

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        Menu optionsMenu = ((PinDetailView) getActivity()).getOptionsMenu();

        optionsMenu.findItem(R.id.action_read).setVisible(true);
        optionsMenu.findItem(R.id.action_browser).setVisible(true);
        optionsMenu.findItem(R.id.action_refresh).setVisible(true);
        optionsMenu.findItem(R.id.action_exit).setVisible(false);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void setRefreshActionButtonState(boolean refreshing) {
        Menu optionsMenu = ((PinDetailView) getActivity()).getOptionsMenu();
        if (optionsMenu == null || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return;
        }

        final MenuItem refreshItem = optionsMenu.findItem(R.id.action_refresh);
        if (refreshItem != null) {
            if (refreshing) {
                refreshItem.setActionView(R.layout.actionbar_progress);
            } else {
                refreshItem.setActionView(null);
            }
        }
    }

}
