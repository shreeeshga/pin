/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.fragments;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SyncStatusObserver;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.TextView;
import in.shreesh.pin.R;
import in.shreesh.pin.activities.License;
import in.shreesh.pin.activities.Login;
import in.shreesh.pin.activities.SettingsActivity;
import in.shreesh.pin.activities.Tags;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.utils.Utils;

public class BasePinFragment extends Fragment {
    private Menu optionsMenu;
    private Object syncObserverHandle;
    private TextView emptyView;
    private SyncStatusObserver syncStatusObserver = new SyncStatusObserver() {
        @Override
        public void onStatusChanged(int which) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Account[] accounts = AccountManager.get(getActivity().getApplicationContext())
                            .getAccountsByType("in.shreesh.pin.PinBoard");
                    if (accounts == null || accounts.length == 0) {
                        setRefreshActionButtonState(false);
                        return;
                    }

                    boolean syncActive = ContentResolver.isSyncActive(accounts[0], PinBoardContract.CONTENT_AUTHORITY);
                    boolean syncPending = ContentResolver.isSyncPending(accounts[0],
                                                                        PinBoardContract.CONTENT_AUTHORITY);
                    setRefreshActionButtonState(syncActive || syncPending);
                }
            });
        }
    };

    public BasePinFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pager_tab_item, container, false);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (syncObserverHandle != null) {
            ContentResolver.removeStatusChangeListener(syncObserverHandle);
            syncObserverHandle = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        emptyView = (TextView) getView().findViewById(R.id.emptyPins);

        syncStatusObserver.onStatusChanged(0);
        final int mask = ContentResolver.SYNC_OBSERVER_TYPE_PENDING | ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE;
        syncObserverHandle = ContentResolver.addStatusChangeListener(mask, syncStatusObserver);

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void setRefreshActionButtonState(boolean refreshing) {
        if (optionsMenu == null || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return;
        }

        final MenuItem refreshItem = optionsMenu.findItem(R.id.action_refresh);
        if (refreshItem != null) {
            if (refreshing) {
                refreshItem.setActionView(R.layout.actionbar_progress);
                emptyView.setText(getString(R.string.loading_content_message));
            } else {
                refreshItem.setActionView(null);
                emptyView.setText(getString(R.string.nothing_to_show));
            }
        }

    }


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.activity_home, menu);
        optionsMenu = menu;
        menu.findItem(R.id.action_search).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_refresh:
                Utils.triggerRefresh(getActivity().getApplicationContext());
                break;
            case R.id.action_tags:
                tags();
                break;
            case R.id.action_logout:
                logout();
                break;
            case R.id.action_settings:
                settings();
                break;
            case R.id.action_license:
                licenses();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void licenses() {
        Intent intent = new Intent(getActivity(), License.class);
        startActivity(intent);
    }

    private void tags() {
        Intent intent = new Intent(getActivity(), Tags.class);
        startActivity(intent);
    }

    private void logout() {
        Preferences.setToken(getActivity(), "");
        startActivity(new Intent(getActivity(), Login.class));
    }

    private void settings() {
        Intent intent = new Intent(getActivity(), SettingsActivity.class);
        startActivity(intent);
    }

}
