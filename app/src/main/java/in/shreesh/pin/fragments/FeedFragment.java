/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import in.shreesh.pin.R;
import in.shreesh.pin.adapters.FeedNetAdapter;
import in.shreesh.pin.adapters.FeedSpinnerAdapter;
import in.shreesh.pin.api.PinBoardAPIFeed;
import in.shreesh.pin.model.FeedPost;
import in.shreesh.pin.utils.PinFilterFactory;
import in.shreesh.pin.utils.Preferences;
import in.shreesh.pin.views.EditableListView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FeedFragment extends BasePinFragment implements AdapterView.OnItemSelectedListener {

    public static String KEY_URI = "content_uri";
    private Spinner spinner;
    private ListView listView;
    private FeedNetAdapter adapter;
    private FeedSpinnerAdapter spinnerAdapter;
    private Set<String> tagSet = new LinkedHashSet<String>();

    public static FeedFragment newInstance() {
        Bundle bundle = new Bundle();
        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupList();

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem feedItem = menu.findItem(R.id.action_feed);
        MenuItem addFeedItem = menu.findItem(R.id.action_edit_feed);

        feedItem.setVisible(true);
        addFeedItem.setVisible(true);
        spinner = (Spinner) MenuItemCompat.getActionView(feedItem);

        tagSet = new LinkedHashSet<String>(Preferences.getFeedTagSet(getActivity()));
        spinnerAdapter = new FeedSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, new ArrayList<String>(tagSet));
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_edit_feed:
                View promptsView = LayoutInflater.from(getActivity()).inflate(R.layout.feed_list_view, null);
                final EditText editText = (EditText) promptsView.findViewById(R.id.editText);
                final EditableListView editListView = (EditableListView) promptsView.findViewById(R.id.feed_list);
                editListView.setList(new ArrayList<String>(tagSet));

                ImageView imageView = (ImageView) promptsView.findViewById(R.id.done_icon);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String value = editText.getText().toString().toLowerCase();
                        if (tagSet.contains("t:" + value)) {
                            Toast.makeText(getActivity(), R.string.tag_exists, Toast.LENGTH_LONG).show();
                            return;
                        } else if (TextUtils.isEmpty(value)) {
                            Toast.makeText(getActivity(), R.string.tag_empty, Toast.LENGTH_LONG).show();
                            return;
                        }

                        tagSet.add("t:" + value);
                        editListView.getAdapter().add("t:" + value);
                        editText.setText("");
                    }
                });

                AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                b.setCancelable(false)
                        .setPositiveButton("Done",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        tagSet.clear();
                                        tagSet.addAll(editListView.getAdapter().getList());
                                        spinnerAdapter.clear();
                                        spinnerAdapter.addAll(tagSet);
                                        spinnerAdapter.notifyDataSetChanged();

                                        Preferences.setFeedTagSet(getActivity(), tagSet);

                                        if (!spinnerAdapter.isEmpty())
                                            fetch(spinnerAdapter.getFeed(0));
                                    }
                                }
                        )
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        tagSet.clear();
                                        tagSet.addAll(new LinkedHashSet<String>(Preferences.getFeedTagSet(getActivity())));
                                        spinnerAdapter.clear();
                                        spinnerAdapter.addAll(tagSet);
                                        spinnerAdapter.notifyDataSetChanged();

                                        dialog.cancel();
                                    }
                                }
                        );
                b.setView(promptsView);
                AlertDialog dialog = b.create();
                dialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupList() {
        adapter = new FeedNetAdapter(getActivity());

        listView = (ListView) getView().findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setEmptyView(getView().findViewById(R.id.emptyPins));
    }


    private void fetch(String feed) {

        new PinBoardAPIFeed.Builder(Preferences.getToken(getActivity())).fetchFeed(feed,
                new Callback<List<FeedPost>>() {
                    @Override
                    public void success(List<FeedPost> feedPosts, Response response) {
                        adapter.setItems(feedPosts);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getActivity(), R.string.fetch_pin_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
        String feed = spinnerAdapter.getFeed(pos);
        fetch(feed);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}