/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import in.shreesh.pin.R;
import in.shreesh.pin.common.SlidingTabLayout;
import in.shreesh.pin.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

public class PinTabsFragment extends Fragment {
    static final String TAG = "PinTabsFragment";
    private static final String KEY_POSITION = "fragment_tab_position";
    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;
    private List<PagerItem> tabs = new ArrayList<PagerItem>();
    private int currentItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int color = getResources().getColor(R.color.themeColor);

        tabs.add(new PagerItem(getString(R.string.tab_all), getResources().getColor(R.color.themeColor), color, 0));
        tabs.add(new PagerItem(getString(R.string.tab_unread), getResources().getColor(R.color.tabColorUnread), color, 1));
        tabs.add(new PagerItem(getString(R.string.tab_private), getResources().getColor(R.color.tabColorPrivate), color, 2));
        tabs.add(new PagerItem(getString(R.string.tab_feeds), getResources().getColor(R.color.themeColor), color, 3));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_POSITION,currentItem);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pager_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(new PagerAdapter(getChildFragmentManager()));
        if (savedInstanceState != null) {
            currentItem = savedInstanceState.getInt(KEY_POSITION);
        } else {
            currentItem = Preferences.getDefaultTab(getActivity());
        }
        viewPager.setCurrentItem(currentItem);

        slidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        slidingTabLayout.setViewPager(viewPager);
        slidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
                currentItem = i;
            }

            @Override
            public void onPageSelected(int i) {
                currentItem = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return tabs.get(position).getIndicatorColor();
            }

            @Override
            public int getDividerColor(int position) {
                return tabs.get(position).getDividerColor();
            }

        });
    }

    static class PagerItem {
        private final CharSequence title;
        private final int indicationColor;
        private final int dividerColor;
        private final int index;

        PagerItem(CharSequence title, int indicatorColor, int dividerColor, int index) {
            this.title = title;
            this.indicationColor = indicatorColor;
            this.dividerColor = dividerColor;
            this.index = index;
        }

        Fragment createPinFragment() {
            return PinFragment.newInstance(index);
        }

        Fragment createFeedFragment() {
            return FeedFragment.newInstance();

        }

        CharSequence getTitle() {
            return title;
        }

        int getIndicatorColor() {
            return indicationColor;
        }

        int getDividerColor() {
            return dividerColor;
        }
    }

    class PagerAdapter extends FragmentPagerAdapter {
        @Override
        public long getItemId(int position) {
            return position;
        }

        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i > 2) return tabs.get(i).createFeedFragment();
            else return tabs.get(i).createPinFragment();
        }

        @Override
        public int getCount() {
            return tabs.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs.get(position).getTitle();
        }


    }
}
