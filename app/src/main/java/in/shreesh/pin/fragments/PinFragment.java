/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.shreesh.pin.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.*;
import android.widget.ListView;
import android.widget.SearchView;
import in.shreesh.pin.R;
import in.shreesh.pin.adapters.BaseCursorAdapter;
import in.shreesh.pin.adapters.PinAdapter;
import in.shreesh.pin.provider.PinBoardContract;
import in.shreesh.pin.utils.PinFilterFactory;


public class PinFragment extends BasePinFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";
    private static final String KEY_SELECTION = "fragment_selection";
    private static final String KEY_SELECTION_ARGS = "fragment_selection_args";
    private static final String KEY_URI = "fragment_content_uri";
    private static final String KEY_SHOW_SEARCH = "fragment_search";
    private BaseCursorAdapter adapter;
    private String selection;
    private String[] selectionArgs;
    private boolean showSearch = false;
    private Menu optionsMenu;
    private PinSearchView searchView;
    private String currentFilter;

    public static PinFragment newInstance(int index) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_SELECTION, PinFilterFactory.getSelection(index));
        bundle.putStringArray(KEY_SELECTION_ARGS, PinFilterFactory.getSelectionArgs(index));
        bundle.putBoolean(KEY_SHOW_SEARCH, index == 0);
        PinFragment fragment = new PinFragment();
        fragment.setArguments(bundle);

        return fragment;
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        selection = args.getString(KEY_SELECTION);
        selectionArgs = args.getStringArray(KEY_SELECTION_ARGS);
        showSearch = args.getBoolean(KEY_SHOW_SEARCH);
        setupList();

        getLoaderManager().restartLoader(0, null, this);
        setHasOptionsMenu(true);
    }

    private void setupList() {
        int[] ids = new int[]{R.id.title, R.id.date, R.id.tags};
        adapter = new PinAdapter(getActivity(), R.layout.card_list_item, null, PinBoardContract.POSTS_PROJECTION, ids,
                                 0);
        ListView listView = (ListView) getView().findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setEmptyView(getView().findViewById(R.id.emptyPins));
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        if (showSearch) {
            menu.findItem(R.id.action_search).setVisible(true);

            optionsMenu = menu;
            MenuItem menuItem = menu.findItem(R.id.action_search);

            searchView = new PinSearchView(getActivity());
            searchView.setOnQueryTextListener(this);

            searchView.setIconifiedByDefault(true);
            searchView.setQueryHint(getResources().getString(R.string.search_hint));
            menuItem.setActionView(searchView);


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        if (!TextUtils.isEmpty(searchView.getQuery())) {
                            searchView.setQuery(null, true);
                            currentFilter = null;
                            getActivity().getSupportLoaderManager().restartLoader(0, null, PinFragment.this);
                        }
                        return true;
                    }
                });
            }
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String newFilter = !TextUtils.isEmpty(newText) ? newText : null;
        if (currentFilter == null && newFilter == null) {
            return true;
        }
        if (currentFilter != null && currentFilter.equals(newFilter)) {
            return true;
        }
        currentFilter = newFilter;

        getActivity().getSupportLoaderManager().restartLoader(0, null, this);
        return true;
    }

    @Override
    public boolean onClose() {
        getActivity().getSupportLoaderManager().restartLoader(0, null, this);
        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri baseUri;
        String filterSelection = selection;
        String[] filterSelectionArgs = selectionArgs;

        if (currentFilter != null) {
            baseUri = PinBoardContract.SEARCH_CONTENT_URI;
            filterSelection = PinBoardContract.Post.COLUMN_NAME_DESC + " LIKE ?" + " OR " + PinBoardContract.Post.COLUMN_NAME_TAGS + " LIKE ?" + " OR " + PinBoardContract.Post.COLUMN_NAME_NOTES + " LIKE ?";
            filterSelectionArgs = new String[]{"%" + currentFilter + "%", "%" + currentFilter + "%", "%" + currentFilter + "%"};
        } else {
            baseUri = PinBoardContract.POSTS_CONTENT_URI;
        }

        return new CursorLoader(getActivity(), baseUri, PinBoardContract.POSTS_PROJECTION, filterSelection,
                                filterSelectionArgs, PinBoardContract.Post.COLUMN_NAME_TIME + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (adapter != null) adapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (adapter != null) adapter.changeCursor(null);
    }

    public static class PinSearchView extends SearchView {
        public PinSearchView(Context context) {
            super(context);
        }

        // The normal SearchView doesn't clear its search text when
        // collapsed, so we will do this for it.
        @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        @Override
        public void onActionViewCollapsed() {
            setQuery("", false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) super.onActionViewCollapsed();
        }
    }
}
