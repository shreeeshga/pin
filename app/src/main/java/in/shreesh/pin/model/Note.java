/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.model;

import java.util.Date;

public class Note {
    String id;
    String title;
    String hash;
    Date created_at;
    Date updated_at;
    String length;

    static class NoteRequest {
        String authToken;
        String format;

        NoteRequest(String authToken, String format) {
            this.authToken = authToken;
            this.format = format;
        }
    }
}
