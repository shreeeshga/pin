/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.model;

import java.util.List;

public class FeedPost {
    public String u;
    public String d;
    public String n;
    public String dt;
    public String a;
    public List<String> t;

    public String getTagString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (String tag : t)
            stringBuilder.append(tag).append(" ");
        return stringBuilder.toString();
    }


}
