/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PinBoardContext {
    private HashMap<String, String> tagsMap = new HashMap<String, String>();
    private HashMap<String, Post> postsMap = new HashMap<String, Post>();

    private ArrayList<Post> posts = new ArrayList<Post>();
    private ArrayList<Note> notes = new ArrayList<Note>();

    public HashMap<String, String> getTags() {
        return tagsMap;
    }

    public void setTags(HashMap<String, String> tags) {
        this.tagsMap = tags;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public ArrayList<Post> getPosts(String tag) {
        ArrayList<Post> t = new ArrayList<Post>();
        for (Post p : posts) {
            if (p.tags != null && p.tags.contains(tag)) t.add(p);
        }
        return t;
    }


    public void setPosts(List<Post> posts) {
        this.posts.clear();
        this.posts.addAll(posts);
    }
}
