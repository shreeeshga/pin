package in.shreesh.pin.common;

import android.util.Log;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * shreesh (shreeshga@gmail.com)
 */
public class ArticleFormatter {

    private static final String[] filters = {"Posted by", "Related ", "Print ", "by ", "By ", "Share "};
    private static final String TAG = "ArticleFormatter";
    private static final int THRESHOLD = 3;
    private String title;
    private String article;
    private String content;

    public ArticleFormatter(String title, String article) {
        this.title = title;
        this.article = article;
        process();
    }


    private void process() {
        String titleString = StringUtils.split(title, "[-:|]")[0];
        LinkedList<String> lines = new LinkedList<String>(Arrays.asList(StringUtils.split(article, "\n")));

        if (lines.isEmpty()) return;

        Log.i(TAG, titleString);

        content = StringUtils.join(filterLeadLines(lines).toArray(), "\n");
    }

    private LinkedList<String> filterLeadLines(LinkedList<String> lines) {
        int threshold = THRESHOLD > lines.size() ? lines.size() : THRESHOLD;
        for (int i = 0; i < threshold; i++) {
            //Log.i(TAG, lines.get(i));
            //Log.i(TAG, "--");

            for (String filter : filters) {
                if (lines.get(i).startsWith(filter) || lines.get(i).contains(title.trim())) {
                    lines.remove(lines.get(i));
                }
            }
        }

        return lines;
    }

    public String getContent() {
        return content;
    }
}
