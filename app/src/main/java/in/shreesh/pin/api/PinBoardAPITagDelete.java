/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

import static in.shreesh.pin.utils.Utils.adapter;

/**
 * Created by shreesh on 7/21/14.
 */
public interface PinBoardAPITagDelete {
    @GET("/tags/delete")
    void delete(@Query("auth_token") String token, @Query("tag") String tag,@Query("format") String format, Callback<HashMap<String,String>> callback);

    static class Builder {
        String authToken;
        String tag;
        String format = "json";

        public Builder(String authToken,String tag) {
            this.authToken = authToken;
            this.tag = tag;
        }

        private PinBoardAPITagDelete buildRetrofit() {
            return adapter().create(PinBoardAPITagDelete.class);
        }

        public void delete(Callback<HashMap<String,String>> callback) {
            buildRetrofit().delete(authToken, tag,format, callback);
        }
    }

}
