/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Query;

import static in.shreesh.pin.utils.Utils.adapter;

public interface PinBoardAPIToken {
    @GET("/user/api_token")
    TokenResponse token(@Header("Authorization") String authToken, @Query("format") String format);

    static class Builder {

        public Builder() {
        }

        private PinBoardAPIToken buildRetrofit() {
            return adapter().create(PinBoardAPIToken.class);
        }

        public TokenResponse fetch(String authToken, String format) {
            return buildRetrofit().token(authToken, format);
        }
    }

    public static class TokenResponse {
        public String result;

        public TokenResponse(String result) {
            this.result = result;
        }
    }

}
