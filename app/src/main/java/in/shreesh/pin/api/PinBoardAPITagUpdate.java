/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

import static in.shreesh.pin.utils.Utils.adapter;

/**
 * Created by shreesh on 7/21/14.
 */
public interface PinBoardAPITagUpdate {
    @GET("/tags/rename")
    void update(@Query("auth_token") String token,
                                   @Query("old") String oldName,
                                   @Query("new") String newName,
                                   @Query("format") String format,
                                   Callback<HashMap<String,String>> callback);

    static class Builder{
        String authToken;
        String oldTag;
        String newTag;
        String format = "json";

        public Builder(String authToken,String oldTag,String newTag) {
            this.authToken = authToken;
            this.oldTag = oldTag;
            this.newTag = newTag;
        }

        private PinBoardAPITagUpdate buildRetrofit() {
            return adapter().create(PinBoardAPITagUpdate.class);
        }

        public void update(Callback<HashMap<String,String>> callback) {
             buildRetrofit().update(authToken, oldTag,newTag, format,callback);
        }
    }
}
