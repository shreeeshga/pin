/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import android.text.TextUtils;

import java.util.List;

import in.shreesh.pin.model.FeedPost;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

import static in.shreesh.pin.utils.Utils.adapterForFeed;

public interface PinBoardAPIFeed {

    @GET("/json/{feed}")
    void feedForTag(@Path("feed") String feed, Callback<List<FeedPost>> callback);

    static class Builder extends PinBoardAPIBuilder {

        public Builder(String authToken) {
            super(authToken);
        }

        private PinBoardAPIFeed buildRetrofit() {
            return adapterForFeed().create(PinBoardAPIFeed.class);
        }

        public void fetchFeed(String feed, Callback<List<FeedPost>> callback) {
            buildRetrofit().feedForTag(TextUtils.htmlEncode(feed), callback);
        }


    }

}
