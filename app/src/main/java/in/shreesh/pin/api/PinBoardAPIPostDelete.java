/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.HashMap;

import static in.shreesh.pin.utils.Utils.adapter;


public interface PinBoardAPIPostDelete {
    @GET("/posts/delete")
    void delete(@Query("auth_token") String token, @Query("url") String url,@Query("format") String format,Callback<HashMap<String,String>> callback);

    static class Builder {
        String authToken;
        String url;
        String format = "json";

        public Builder(String authToken,String url) {
            this.authToken = authToken;
            this.url = url;
        }

        private PinBoardAPIPostDelete buildRetrofit() {
            return adapter().create(PinBoardAPIPostDelete.class);
        }

        public void delete(Callback<HashMap<String,String>> callback) {
            buildRetrofit().delete(authToken, url, format,callback);
        }
    }
}
