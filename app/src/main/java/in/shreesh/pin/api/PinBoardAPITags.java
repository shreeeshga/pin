/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import retrofit.http.GET;
import retrofit.http.Query;

import java.util.HashMap;

import static in.shreesh.pin.utils.Utils.adapter;

public interface PinBoardAPITags {
    @GET("/tags/get")
    HashMap<String, String> tags(@Query("auth_token") String token, @Query("format") String format);

    static class Builder extends PinBoardAPIBuilder {

        public Builder(String authToken) {
            super(authToken);
        }

        private PinBoardAPITags buildRetrofit() {
            return adapter().create(PinBoardAPITags.class);
        }

        public HashMap<String, String> fetchTags() {
            try {
                return buildRetrofit().tags(authToken, format);
            } catch (Exception e) {
                return new HashMap<String, String>();
            }
        }
    }

}
