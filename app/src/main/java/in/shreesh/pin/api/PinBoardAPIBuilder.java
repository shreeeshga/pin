/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import java.util.HashMap;
import java.util.List;

import in.shreesh.pin.model.Post;
import in.shreesh.pin.model.TagSuggetion;
import retrofit.Callback;

public abstract class PinBoardAPIBuilder {
    String authToken;
    String tag;
    String dt;
    String description;
    String extended;
    String url;
    String metaTag;
    String format;
    String shared;
    String toread;
    String replace;

    public PinBoardAPIBuilder(String authToken) {
        this.authToken = authToken;
        this.tag = "";
        this.dt = "";
        this.description = "";
        this.url = "";
        this.metaTag = "";
        this.format = "json";
        this.replace =  "no";
        this.shared = "yes";
        this.toread = "yes";
    }

    public String getReplace() {
        return replace;
    }

    public PinBoardAPIBuilder setReplace(String replace) {
        this.replace = replace;
        return this;
    }

    public String getShared() {
        return shared;
    }

    public PinBoardAPIBuilder setShared(String shared) {
        this.shared = shared;
        return this;
    }

    public String getToread() {
        return toread;
    }

    public PinBoardAPIBuilder setToread(String toread) {
        this.toread = toread;
        return this;
    }

    public String getExtended() {
        return extended;
    }

    public PinBoardAPIBuilder setExtended(String extended) {
        this.extended = extended;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public PinBoardAPIBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public PinBoardAPIBuilder setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public PinBoardAPIBuilder setDate(String date) {
        this.dt = date;
        return this;
    }

    public PinBoardAPIBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public PinBoardAPIBuilder setMetaTag(String metaTag) {
        this.metaTag = metaTag;
        return this;
    }

    public PinBoardAPIBuilder setFormat(String format) {
        this.format = format;
        return this;
    }

    public void updatePost(Callback<HashMap<String, String>> callback) {
    }

    public List<Post> fetch() {
        return null;
    }

    public void fetchSuggest(Callback<List<TagSuggetion>> callback) {

    }
}
