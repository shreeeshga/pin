/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import in.shreesh.pin.model.Note;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.ArrayList;
import java.util.List;

import static in.shreesh.pin.utils.Utils.adapter;


public interface PinBoardAPINotes {
    @GET("/notes/get")
    List<Note> notes(@Query("auth_token") String token, @Query("format") String format);

    static class Builder extends PinBoardAPIBuilder {

        public Builder(String authToken) {
            super(authToken);
        }

        private PinBoardAPINotes buildRetrofit() {
            return adapter().create(PinBoardAPINotes.class);
        }

        public List<Note> fetchNotes() {
            try {
                return buildRetrofit().notes(authToken, format);
            } catch (Exception e) {
                return new  ArrayList<Note>(0);
            }
        }


    }
}
