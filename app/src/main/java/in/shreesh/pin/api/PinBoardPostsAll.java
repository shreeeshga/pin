/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import in.shreesh.pin.model.Post;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.ArrayList;
import java.util.List;

import static in.shreesh.pin.utils.Utils.adapter;

public interface PinBoardPostsAll {
    @GET("/posts/all")
    List<Post> posts(@Query("auth_token") String token,
            @Query("tag") String tag,
            @Query("dt") String date,
            @Query("url") String url,
            @Query("meta") String metatag,
            @Query("format") String format);

    static class Builder extends PinBoardAPIBuilder {

        public Builder(String authToken) {
            super(authToken);
        }

        private PinBoardPostsAll buildRetrofit() {
            return adapter().create(PinBoardPostsAll.class);
        }

        public List<Post> fetch() {
            try {
                return buildRetrofit().posts(authToken, tag, dt, url, metaTag, format);
            } catch (Exception e) {
                return new ArrayList<Post>();
            }
        }
    }

}
