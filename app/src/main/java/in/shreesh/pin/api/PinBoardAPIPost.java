/*
 * Copyright (C) 2014 Shreesh Ayachit (shreeshga@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package in.shreesh.pin.api;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.HashMap;

import static in.shreesh.pin.utils.Utils.adapter;

public interface PinBoardAPIPost {
    @GET("/posts/add")
    void add(@Query("auth_token") String token,
            @Query("url") String url,
            @Query("description") String title,
            @Query("extended") String description,
            @Query("tags") String tags,
            @Query("dt") String datetime,
            @Query("replace") String replace,
            @Query("shared") String shared,
            @Query("toread") String toread,
            @Query("format") String format,
            Callback<HashMap<String, String>> callable);

    static class Builder extends PinBoardAPIBuilder {

        public Builder(String authToken) {
            super(authToken);
        }

        private PinBoardAPIPost buildRetrofit() {
            return adapter().create(PinBoardAPIPost.class);
        }

        public void updatePost(Callback<HashMap<String, String>> callback) {
            buildRetrofit().add(authToken, url, description, extended, tag, dt, replace,shared, toread, format, callback);
        }
    }
}
